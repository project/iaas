<?php

/**
 * @file
 * Enables users to access the Publicly managed clouds (Amazon EC2).
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

 
module_load_include('inc', 'amazon_ec2', 'amazon_ec2_constants');

/**
 * Updated by yas   2011/02/14
 * Updated by yas   2011/02/02
 */

/**
 * Implementation of hook_help().
 */
function amazon_ec2_help($section) {

  switch ($section) {
    case 'admin/help#' . AMAZON_EC2_CONTEXT :
      $output = '<p>' . t('The cloud module creates a user interface for users to manage clouds. Users can Create Instances, Describe Instances etc.') . '</p>';

      return $output;
  } 
}


/**
 * Implementation of hook_menu().
 */
function amazon_ec2_menu() {

  // Required for uninstall
  drupal_load('module', 'cloud');
  drupal_load('module', 'aws_ec2_lib');
  
  $items = array();
  $items = aws_ec2_lib_get_menu(AMAZON_EC2_CONTEXT) ;

  return $items;    // hook_menu retuns an array
} 


/**
 * Implementation of hook_perm().
 */
function amazon_ec2_perm() {
  
  // get the permissions from ec2_lib
  $perm = array();
  $perm = aws_ec2_lib_get_perm(AMAZON_EC2_CONTEXT);

  return $perm;    // hook_perm returns array
} 


/**
 * Implementation of hook_admin_settings().
 * This form is used to set the EC2 Information
 */
function amazon_ec2_admin_settings() {
  
  // Get the admin settings from ec2_lib
  $form = aws_ec2_lib_get_admin_settings(AMAZON_EC2_CONTEXT) ;

  return $form ;
} 


/**
 * Implementation of hook_cron().
 * The call hierarchy is as follows:
 *          sub-cloud_cron
 *                       --> aws_ec2_lib_fetch_data( sub-cloud_context)
 *                                                                    --> _aws_ec2_lib_download_all( sub-cloud_context)
 * This cron will fetch information from the server and save it in the database.
 * A REST request is sent for each of the following entities.
 * The response is parsed and database is updated with the information fetched.
 *
 *          Elastic IP Info,
 *          Instances Info,
 *          SSH Keys Info,
 *          Security Group Info,
 *          Zone Info,
 *          Snapshot Info,
 *          Volume Info
 */
function amazon_ec2_cron() {

  aws_ec2_lib_fetch_data(AMAZON_EC2_CONTEXT) ; // This will fetch Cloud related information
  aws_ec2_lib_process_bundle_instance() ;
} 



/**
 * Implementation of hook_cloud_set_info().
 * This hook set's the information about the cloud
 */
function amazon_ec2_cloud_set_info() {

  return array(

    'cloud_name'         => AMAZON_EC2_CONTEXT,
    'cloud_display_name' => AMAZON_EC2_DISPLAY_NAME,
    'instance_types'     => array(
      'm1.small'         => 'm1.small'   ,
      't1.micro'         => 't1.micro'   ,
      'c1.medium'        => 'c1.medium'  ,
      'm1.large'         => 'm1.large'   ,
      'm1.xlarge'        => 'm1.xlarge'  ,
      'c1.xlarge'        => 'c1.xlarge'  ,
      'm2.2xlarge'       => 'm2.2xlarge' ,
      'm2.4xlarge'       => 'm2.4xlarge' ,
      'cc1.4xlarge'      => 'cc1.4xlarge',
      'cg1.4xlarge'      => 'cg1.4xlarge',
    ),

    'cloud_pricing_data' => array(

      't1.micro'    => array(
        'instance_type'      => 't1.micro'       ,
        'description'        => t('Micro')       ,
        'linux_or_unix_cost' => '0.002'          ,
        'windows_cost'       => '0.004'          ,
      ),
    
      'm1.small'    => array(
        'instance_type'      => 'm1.small'          ,
        'description'        => t('Small (Default)'),
        'linux_or_unix_cost' => '0.085'             ,
        'windows_cost'       => '0.090'             ,
      ),

      'c1.medium'   => array(
        'instance_type'      => 'c1.medium',
        'description'        => t('Medium'),
        'linux_or_unix_cost' => '0.17'     ,
        'windows_cost'       => '0.21'     ,
      ),

      'm1.large'    => array(
        'instance_type'      => 'm1.large',
        'description'        => t('Large'),
        'linux_or_unix_cost' => '0.34'    ,
        'windows_cost'       => '0.39'    ,
      ),

      'm1.xlarge'   => array(
        'instance_type'      => 'm1.xlarge'     ,
        'description'        => t('Extra Large'),
        'linux_or_unix_cost' => '0.5'           ,
        'windows_cost'       => '0.7'           ,
      ),
      
      'c1.xlarge'   => array(
        'instance_type'      => 'c1.xlarge'                       ,
        'description'        => t('High-CPU Extra Large Instance'),
        'linux_or_unix_cost' => '0.68'                            ,
        'windows_cost'       => '0.72'                            ,
      ),
    
      'm2.2xlarge'  => array(
        'instance_type'      => 'm2.2xlarge'           ,
        'description'        => t('Double Extra Large'),
        'linux_or_unix_cost' => '1.2'                  ,
        'windows_cost'       => '1.7'                  ,
      ),

      'm2.4xlarge'  => array(
        'instance_type'      => 'm2.4xlarge'              ,
        'description'        => t('Quadruple Extra Large'),
        'linux_or_unix_cost' => '2.4'                     ,
        'windows_cost'       => '2.9'                     ,
      ),

      'cc1.4xlarge' => array(
        'instance_type'      => 'cc1.4xlarge'                                      ,
        'description'        => t('Cluster Compute Quadruple Extra Large Instance'),
        'linux_or_unix_cost' => '1.6'                                              ,
        'windows_cost'       => '2.0'                                              ,
      ),

      'cg1.4xlarge' => array(
        'instance_type'      => 'cg1.4xlarge'                                      ,
        'description'        => t('Cluster GPU Quadruple Extra Large Instance'),
        'linux_or_unix_cost' => '2.1'                                              ,
        'windows_cost'       => '0.0'                                              ,
      ),
    ),
  );
} 


/**
 * Implementation of hook_server_template().
 * This hook displays the server template forms
 */
function amazon_ec2_server_template($op, $params=array() ) {

  switch ($op) {
    
    case 'list':
          return _cloud_server_templates_list('cloud_server_templates', AMAZON_EC2_CONTEXT, $params ) ;  
    case 'create':
          $form =  drupal_get_form('aws_ec2_lib_server_templates_new' , AMAZON_EC2_CONTEXT, $params ) ;     
          return $form ;
    case 'view':
          $form =  drupal_get_form('aws_ec2_lib_server_templates_view', AMAZON_EC2_CONTEXT, $params ) ;  
          return $form;        
    case 'cluster_form':
          return drupal_get_form('amazon_ec2_cluster_form', $params ) ;

  }      
} 


/**
 * Implementation of hook_cloud_action().
 * This hook performs the action for cloud
 */
function amazon_ec2_cloud_action($op, $params=array() ) {

  return aws_ec2_lib_action( AMAZON_EC2_CONTEXT , $op , $params ) ;
}


function amazon_ec2_cluster_form($form, $params) {

  return aws_ec2_lib_cluster_form(AMAZON_EC2_CONTEXT , $form, $params) ;
}


function theme_amazon_ec2_cluster_form($form) {

  return theme_aws_ec2_lib_cluster_form(AMAZON_EC2_CONTEXT, $form) ;
}


function amazon_ec2_cluster_form_validate($form_id, $form_values) {

  aws_ec2_lib_cluster_form_validate(AMAZON_EC2_CONTEXT , $form_id, $form_values ) ;
  return ;
}  


function amazon_ec2_cluster_form_submit($form_id, $form_values) {

  aws_ec2_lib_cluster_form_submit(AMAZON_EC2_CONTEXT, $form_id, $form_values) ;
  return ;
}


function amazon_ec2_theme() {

  return array(
    'amazon_ec2_cluster_form' => array(
      'arguments' => array(
        'form' => NULL
      ), 
    ), 
  );
}


/**
 * Implementation of hook_cloud_get_all_instances().
 */
function amazon_ec2_cloud_get_all_instances($filter=array()) {
  
  $instances_list = _aws_ec2_lib_get_all_instances_db(AMAZON_EC2_CONTEXT , $filter ) ;

  foreach ($instances_list as &$instance ) {
    
    $action_data = aws_ec2_lib_get_instances_action( AMAZON_EC2_CONTEXT , $instance ) ; 
    $instance['action_data'] = $action_data ;
  }
  
  return $instances_list ;
} 

/**
 * Return a List on Instances matching the filter
 */
function amazon_ec2_cloud_get_instance($filter=array()) {

  $instances_list = _aws_ec2_lib_get_all_instances_db(AMAZON_EC2_CONTEXT , $filter );
  return $instances_list;
}

function amazon_ec2_cloud_get_ssh_key($params) {
    return _aws_ec2_lib_get_ssh_key($params);
}


/**
 * Implementation of hook_cloud_update_data().
 */
function amazon_ec2_cloud_update_data() {

    return aws_ec2_lib_fetch_data(AMAZON_EC2_CONTEXT) ;
}


/**
 * Implementation of hook_user().
 */
function amazon_ec2_user($op,   &$edit,   &$account,   $category = NULL) {

  switch ($op) {

    case 'login' : // User Logged in. Check if key is present
      aws_ec2_lib_ssh_keys_check_load(AMAZON_EC2_CONTEXT , $account->name) ;  
      break ;
  }
}


