<?php

/**
 * @file
 * Constants related to amazon_ec2.*
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('AMAZON_EC2_CONTEXT'              , 'amazon_ec2'            ) ;
define('AMAZON_EC2_DISPLAY_NAME'         , 'Amazon EC2'            ) ;  
define('AMAZON_EC2_DEFAULT_TEMPLATE_NAME', ' - Select Template - ' ) ;  
