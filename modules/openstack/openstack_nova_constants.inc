<?php

/**
 * @file Constants related to OpenStack nova.
 * 
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('OPENSTACK_NOVA_CONTEXT'              , 'openstack_nova'            ) ;
define('OPENSTACK_NOVA_DISPLAY_NAME'         , 'OpenStack nova'            ) ;  
define('OPENSTACK_NOVA_DEFAULT_TEMPLATE_NAME', ' - Select Template - ' ) ;  
