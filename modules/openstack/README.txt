BASIC INFO
==========

- Allows to manage an EC2 compatible API version of OpenStack nova cloud.
- Works with Cloud module


HOW TO USE
==========

- Enable OpenStack nova module (Requires Cloud module), then

  i)  Clouds | OpenStack nova | Instaces -> Click - Refresh Page -
  ii) Clouds | OpenStack nova | Images   -> Click - Refresh Page -

- You also need to Setup cron.php on Drupal for automatic update
  for the latest information on OpenStack nova.

- Make 'file' directory writable.
  e.g. If you have /var/www/html/sites/default/files,
       chmod 777 /var/www/html/sites/default/files
      (because it creates files/cloud sub-directory)


How to Install OpenStack on Amazon EC2
======================================

1) Launch an 'm1.large' Instance of 
 - us-east-1     : ubuntu-maverick-10.10-amd64-server-20101007.1 (ami-688c7801) 
 - ap-southeast-1: ubuntu-maverick-10.10-amd64-server-20101007.1 (ami-68136d3a)

2) Open ports in Security Group
   TCP - IPs: 0.0.0.0/0 Port: 22   (for SSH)
   TCP - IPs: 0.0.0.0/0 Port: 8773 (OpenStack nova API uses 8773) 

3) Log into the instance as a username 'ubuntu'

4) Set up a password for root

sudo passwd root

5) Install bzr - Stay in a directory at /home/ubuntu

sudo apt-get install bzr

6) Download OpenStack nova (from trunk, the latest version)

bzr checkout lp:nova

7) Install nova - Stay in a directory at /home/ubuntu

sudo nova/contrib/nova.sh install

8) Run nova - This script will launch 'multiple screens' of screen command.

sudo nova/contrib/nova.sh run

9) Teminate nova 

sudo nova/contrib/nova.sh terminate
sudo nova/contrib/nova.sh clean or

Ctrl + A D


DIRECTORY STRUCTURE
===================

aws
  +-aws (depends on REST Client module)
  +-modules (Will be called by other EC2 API compatible clouds like Eucalyptus,  OpenStack nova and etc.)
    +-aws_ec2
      - aws_ec2_api.module (depends on aws_common) 
      - aws_ec2_lib.module (depends on ec2_api)
      - << EBS Volume wrapper     >> (.inc)
      - << Elastic IP wrapper     >> (.inc)
      - << Images wrapper         >> (.inc)
      - << Instances wrapper      >> (.inc)
      - << Register Image wrapper >> (.inc)
      - << Security Group wrapper >> (.inc)
      - << Snapshot wrapper       >> (.inc)
      - << SSH Key>> (User Keys Management based on Permission) wrapper (.inc)
      - ...

iaas
  +-modules
    +-openstack

rest_client


CHANGE HISTORY
==============
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org (Working!)
2010/12/26 ver.0.81 released to reviewing process of drupal.org (Didn't work)
2010/12/15 ver.0.8  released to reviewing process of drupal.org (Didn't work)
2010/11/09 ver.0.7  released to reviewing process of drupal.org (Didn't work)


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt