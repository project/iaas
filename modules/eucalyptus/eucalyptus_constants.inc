<?php

/**
 * @file Constants related to Eucalyptus.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

define('EUCALYPTUS_CONTEXT'              , 'eucalyptus'            ) ;
define('EUCALYPTUS_DISPLAY_NAME'         , 'Eucalyptus'            ) ;  
define('EUCALYPTUS_DEFAULT_TEMPLATE_NAME', ' - Select Template - ' ) ;  
