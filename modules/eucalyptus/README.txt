BASIC INFO
==========

- This module is a sub-cloud system under Cloud module
- It provides almost all of Amazon EC2 functionalities


SYSTEM REQUIREMENTS
===================
- 512MB Memory: If you use Amazon EC2 module, the running host of this
  system requires more than 512MB memory to download a list of images
 (because it's huge amount of data for listing).


HOW TO USE
==========

- Enable Eucalyptus module (Requires Cloud module), then

  i)  Clouds | Amazon EC2 | Instaces -> Click - Refresh Page -
  ii) Clouds | Amazon EC2 | Images   -> Click - Refresh Page -

- You also need to Setup cron.php on Drupal for automatic update
  for the latest information on Eucalyptus.

- Make 'file' directory writable.
  e.g. If you have /var/www/html/sites/default/files,
       chmod 777 /var/www/html/sites/default/files
      (because it creates files/cloud sub-directory)


LIMITATION
==========

- Only sigle region is supported (e.g. only east-1x Availability Zone)
- Some of features are not implemented


SSH CONSOLE SETUP
=================

- Change a constant value 'CLOUD_SEED' in ec2_lib_constants.inc to your 
  favorite keyword.
- The security is not considered in current implementation. Please be
  aware of your private key will pass through via the Internet if
  you deploy this system on the Internet.
- Go to Cloud | Amazon EC2 | SSH Keys and edit / update your private key
- If you input your private key through the above menu, the SSH console
  will not work.
- You need to get MindTerm from http://www.appgate.com/mindtermcontact
  and put mindterm.jar file into your <wwwroot> folder.  (e.g. /var/www/html)
- Launch an Amazon EC2 instance and make sure the instance is running. Then
  click 'console' icon or Console tab in Instance Detail page.
- You need to trust two Java Applets on both mindterm.jar and CloudSSHKeyManager.jar
  by clicking 'Allow' or 'Yes' button in order to handle / transfer your
  private key.
- If you see the following error:

  e.g. Error when setting up authentication:
       C:\Users\<USERNAME>\Application Data\MindTerm\64656661756c74

  then that error message means you skipped CloudSSHKeyManager.jar authentication.
  Please clear Java applet cache from Java control panel and re-launch your
  browser.
- If you are using basic authentication on your site, that would be a problem to
  use this functionality due to the permission.


MOUDLE DEPENDENCY
=================

This module works with the following modules:

- Cloud
- Server Templates
- Amazon EC2 Library
- Amazon EC2 API
- AWS Common
- REST Client
 

DIRECTORY STRUCTURE FOR AWS MODULE FAMILY
=========================================

aws
  +-aws (depends on REST Client module)
  *-amazon_ec2              (This module: depends on ec2_api)(Amazon EC2        is part of AWS)
  x-amazon s3               (depends on s3_api              )(Amazon S3         is part of AWS)
  x-amazon_smpledb          (depends on simpledb_api        )(Amazon SimpleDB   is part of AWS)
  x-amazon_cloud_watch      (depends on cloud_watch_api     )(Amazon CloudWatch is part of AWS)
  +-lib (Will be called by other EC2 API compatible clouds like Eucalyptus,  OpenStack nova and etc.)
    +-ec2 
      - ec2_api.module (depends on aws_common) 
      - ec2_lib.module (depends on ec2_api)
      - EBS Volume wrapper (.inc)
      - Elastic IP wrapper (.inc)
      - Images wrapper (.inc)
      - Instances wrapper (.inc)
      - Register Image wrapper (.inc)
      - Security Group wrapper (.inc)
      - Snapshot wrapper (.inc)
      - SSH Key (User Keys Management based on Permission) wrapper (.inc)
      - ...

iaas
  +-modules
    +-eucalyptus

rest_client


x... NOTE: NOT IMPLEMENTED.  SHOWN ONLY AS A REFERENCE AND PROPOSED STRUCTURE SPEC.



CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt