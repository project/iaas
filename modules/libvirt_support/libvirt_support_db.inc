<?php 

/**
 * @file libvirt Support Database support include file.
 * This cloud type list is mainly used to check whether the Cloud specific Module is enabled or no.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

module_load_include('inc', 'libvirt_support', 'libvirt_support_constants');

function _libvirt_support_download_instance_info() {

  $myclient = _libvirt_support_describe_instances() ;

  $obj_response = new SimpleXMLElement($myclient -> data);

  $id_user_arr = cloud_get_all_nickname('vmid', 'assigned_to', LIBVIRT_SUPPORT_TABLE) ;
  db_query('truncate table {' . LIBVIRT_SUPPORT_TABLE . '}' ,  NULL);

  $insert_query = 'INSERT INTO {' . LIBVIRT_SUPPORT_TABLE . "} 
                  ( `vmid` ,  
                    `vm_name` ,  
                    `vm_ip` ,  
                    `host_ip` ,  
                    `vm_state` ,  
                    `assigned_to` 
                  )  values";

  $count = 0 ;

  $query_args = array() ;

  foreach ($obj_response -> hostSet -> host as $host_node) {

    if (isset($host_node -> ip) == FALSE) {
      continue ;
    } 

    $host_ip = $host_node -> ip ;

    foreach ($host_node -> guestSet -> guest as $guest_node) {

      $query_args[] = trim($guest_node -> uuid) ;
      $query_args[] = trim($guest_node -> domain) ;
      $query_args[] = trim($guest_node -> ip);
      $query_args[] = trim($host_ip) ;
      $query_args[] = trim($guest_node -> state) ;

      $tmp_user_name = cloud_check_in_array($id_user_arr,  trim($guest_node -> uuid)) ;
      $query_args[] = $tmp_user_name != NULL ? $tmp_user_name : '' ;

      $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s', '%s', '%s' ) ," ;

      $count++ ;
    } 
  } 

  if ($count == 0)
    return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query($insert_query , $query_args);
  return TRUE;
} 

/**
 * Download information from libvirt
 */
function _libvirt_support_describe_instances() {

  $host    = variable_get('libvirt_host_uri', '');
  $request = array(
    'Action'  => 'GetData', 
    'Version' => '1'
  );

  return aws_query_request($cloud_context, $host_uri, $request) ;
} 


function _libvirt_support_get_describe_instances_query() {

  global $user;
  $owner = $user->name ;
  return $query = 'SELECT c.* FROM {' . LIBVIRT_SUPPORT_TABLE . "} c WHERE assigned_to='$owner'  and %s like '%%%s%%' " ;
}