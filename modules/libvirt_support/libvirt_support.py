#!/usr/bin/python

#$id$

##########################################################################
#
# Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
#
##########################################################################

from mod_python import apache;
import libvirt;
from xml.dom.minidom import Document
import os

__author__ = 'Ram Parashar'
__date__ = "$id$"

HOST_CONF_FILE = 'libvirt_support.conf'

class LibvirtCloudManager:

  def getConfValue(self,  key):
    for line in open(HOST_CONF_FILE,  'r').readlines():
      left,  right = line.split('=')
      if (left.strip(' ') == key):
        return right.strip(' ')

  def getHosts(self):
    return self.getConfValue('hosts').strip(' \n').split(' ');

  def collectHostDetails(self):
    state_names = {
      libvirt.VIR_DOMAIN_RUNNING  : 'running', 
      libvirt.VIR_DOMAIN_BLOCKED  : 'idle', 
      libvirt.VIR_DOMAIN_PAUSED   : 'paused', 
      libvirt.VIR_DOMAIN_SHUTDOWN : 'in shutdown', 
      libvirt.VIR_DOMAIN_SHUTOFF  : 'shut off', 
      libvirt.VIR_DOMAIN_CRASHED  : 'crashed', 
      libvirt.VIR_DOMAIN_NOSTATE  : 'no state'}

    hosts = self.getHosts();

    doc = Document();
    rootDoc = doc.createElement('root');
    doc.appendChild(rootDoc);

    hostSet = doc.createElement('hostSet');
    rootDoc.appendChild(hostSet);


    for host in hosts:
      conn = '';
      try:
          conn = libvirt.openReadOnly('qemu+ssh://root@' + host + '/system');
      except:
          print 'Could not open: Connection to Host';

      if conn == None:
          print 'Failed to open connection to the hypervisor'
          continue
      else:
        # print conn.getCapabilities();

        host = doc.createElement('host');
        hostSet.appendChild(host);

        hostIp = doc.createElement('ip');
        host.appendChild(hostIp);
        hostIpVal = doc.createTextNode(str(host))
        hostIp.appendChild(hostIpVal)

        guestSet = doc.createElement('guestSet');
        host.appendChild(guestSet);

        for id in conn.listDomainsID():
          guest = doc.createElement('guest');
          guestSet.appendChild(guest);

          domain = conn.lookupByID(id)
          info = domain.info();

          element = doc.createElement('domain')
          guest.appendChild(element);
          elementVal = doc.createTextNode(domain.name())
          element.appendChild(elementVal)

          element = doc.createElement('uuid')
          guest.appendChild(element);
          elementVal = doc.createTextNode(domain.UUIDString())
          element.appendChild(elementVal)

          element = doc.createElement('state')
          guest.appendChild(element);
          elementVal = doc.createTextNode(state_names[info[0]])
          element.appendChild(elementVal)

          element = doc.createElement('ip')
          guest.appendChild(element);
          elementVal = doc.createTextNode(self.getIpAddress(domain.name()))
          element.appendChild(elementVal)

        conn.close();

    return doc.toprettyxml(indent='  ');

  def usage():
    print "Usage: %s HOSTNAME" % sys.argv[0];
    print '       List active domains of HOSTNAME and print some info';

  def print_section(title):
    print "\n%s" % title;
    print "=" * 60;

  def print_entry(key,  value):
      print "%-10s %-10s" % (key,  value);

  def print_xml(key,  ctx,  path):
    res = ctx.xpathEval(path);
    if res is None or len(res) == 0:
      value = 'Unknown';
    else:
      value = res[0].content;
      print_entry(key,  value);
    return value;

  def getIpAddress(self,  hostName):
    os.system ('nmblookup ' + hostName + ' > temp', );
    for line in open('temp',  'r').readlines():
      if (self.contains(line ,  hostName + '<00>')):
        left,  right = line.split(' ')
        return left

  def contains(self,  theString,  theQueryValue):
    return theString.find(theQueryValue) > -1

def handler(req):
  req.log_error('handler')
  req.content_type = 'text/html'
  req.send_http_header()
  req.write('<html><head><title>Testing mod_python</title></head><body>')
  req.write('Libvirt Cloud Manager')
  req.write('</body></html>')
  req.write
  return apache.OK

if  __name__ == '__main__':
  libvirtCloudManager = LibvirtCloudManager();
  print libvirtCloudManager.collectHostDetails();
