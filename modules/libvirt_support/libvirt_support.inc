<?php 

/**
 * @file Enables users to access the Publicly managed clouds.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas 2011/02/02
 */

function _libvirt_support_instances($cloud_context) {

  
  return drupal_get_form('libvirt_support_instances_details') ;
} 

function libvirt_support_instances_details($form_submit='') {

  $form['options'] = array(
    '#type'   => 'fieldset',
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
 // '#title'  => t('Filter'), 
  );

  $options = array();

  $options[] = 'Nickname' ;
  $options[] = 'Id' ;
  $options[] = 'Hostname' ;

  global $user;
  $filter     = cloud_get_filter_value( $form_submit , 'filter' ) ;
  $filter_col = cloud_get_filter_value( $form_submit , 'operation' ) ;
  $filter     = trim($filter);

  if ($filter_col == 0) {
    $column = 'Nickname' ;
    $sql_col = 'vm_name' ;
  }
  elseif ($filter_col == 1) {
    $column = 'Id' ;
    $sql_col = 'vmid' ;
  }
  elseif ($filter_col == 2) {
    $column = 'Public-DNS-Name' ;
    $sql_col = 'vm_ip' ;
  } 

  $query_args = array() ;
  if (isset($filter)) {
    $query_args[] = $sql_col ;
    $query_args[] = $filter ;
  }
 else {
    $filter = ' 1 ' ;
    $query_args[] = 'vm_name' ;
    $query_args[] = '' ;
  } 

  asort($options);
  $form['options']['label'    ] = array( '#type' => 'item'     , '#title'   => t('Filter'));
  $form['options']['operation'] = array( '#type' => 'select'   , '#options' => $options, '#default_value' => $filter_col);
  $form['options']['filter'   ] = array( '#type' => 'textfield', '#size'    => 40      , '#default_value' => $filter);
  $form['options']['submit'   ] = array( '#type' => 'submit'   , '#value'   => t('Apply'));

  $form['header'] = array('#type' => 'value', 
    '#value' => array(
    array('data' => t('Nickname'  ) ,  'field' => 'vm_name', 'sort' => 'asc'), 
    array('data' => t('ID'        ) ,  'field' => 'vmid'          ), 
    array('data' => t('Hostname'  ) ,  'field' => 'vm_ip'          ), 
    array('data' => t('State'     ) ,  'field' => 'vm_state'       ), 
    array('data' => t('Host'      ) ,  'field' => 'host_ip'        ), 
    array('data' => t('User'      ) ,  'field' => 'assigned_to')
    )
  );

  $query = _libvirt_support_get_describe_instances_query() ;
  $query .= tablesort_sql($form['header']['#value']) ;

  $result = pager_query($query, LIBVIRT_PAGER_LIMIT, 0, NULL, $query_args);

  while ($node = db_fetch_object($result)) {

    $form['Nickname'  ][$node -> vmid] = array(array( '#value' => t($node -> vm_name        )));
    $form['ID'        ][$node -> vmid] = array(array( '#value' => t($node -> vmid          )));
    $form['Public_DNS'][$node -> vmid] = array(array( '#value' => t($node -> vm_ip          )));
    $form['State'     ][$node -> vmid] = array(array( '#value' => t($node -> vm_state       )));
    $form['Host'      ][$node -> vmid] = array(array( '#value' => t($node -> host_ip        )));
    $form['User'      ][$node -> vmid] = array(array( '#value' => t($node -> assigned_to)));
  } 

  $form['pager'] = array('#value' => theme('pager', NULL, LIBVIRT_PAGER_LIMIT, 0));

  return $form;
} 

function theme_libvirt_support_instances_details($form) {

  foreach (element_children($form['Nickname']) as $key) {

    $rows[] = array(
      drupal_render($form['Nickname'  ][$key]),
      drupal_render($form['ID'        ][$key]),
      drupal_render($form['Public_DNS'][$key]),
      drupal_render($form['State'     ][$key]),
      drupal_render($form['Host'      ][$key]),
      drupal_render($form['User'      ][$key]),
    );
  } 

  $output  = drupal_render($form['options']);
  $output .= theme('table', $form['header']['#value'], $rows);

  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  } 

  $output .= drupal_render($form);
  return $output;
} 

function libvirt_support_instances_details_submit($form_id, $form_values) {

  $form_values = $form_values['values'];
  if ($form_values['op'] == t('Apply')) {

  } 

  return;
} 

function _libvirt_support_get_libvirt_image_options($cloud_context) {

  $image_options = array() ; 
  
  $result = db_query('SELECT * FROM {cloud_' . $cloud_context . "_info} where assigned_to = '' and vm_state='Halted' ");
  while ($nodes = db_fetch_object($result)) {
      $image_options[$nodes->vmid] = $nodes->vm_name . ' ( ' .  $nodes->host_ip . ' )';
  }
  
  $image_options[LIBVIRT_CLOUD_NONE] = LIBVIRT_CLOUD_NONE ;
  
  asort($image_options);
  return $image_options ;
}


function _libvirt_support_server_template($cloud) {
  
  // Add your implementation here
  
  
  return $form ;
}

function _theme_libvirt_support_server_templates_new($form, $cloud_type) {
  
  // Add your implementation here
  
  return $output ;
}


function _libvirt_support_server_templates_view($cloud_context) {

  // Add your implementation here for Server Templates
  return $form ;
}
