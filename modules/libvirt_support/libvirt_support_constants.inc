<?php

/**
 * @file libvirt Support constants include file.
 * This cloud type list is mainly used to check whether the Cloud specific Module is enabled or no.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */
 
define('LIBVIRT_CLOUD_PREFIX', 'cloud_'  ) ;
define('LIBVIRT_CLOUD_NONE'  , '- none -') ;

define('LIBVIRT_CLOUD'            , 'libvirt_cloud'                         ) ;
define('LIBVIRT_PATH'             , 'clouds/libvirt'                        ) ;
define('LIBVIRT_SUPPORT_TABLE'    , LIBVIRT_CLOUD_PREFIX . 'libvirt_support') ;
define('LIBVIRT_CLOUD_MODULE_NAME', 'libvirt_support'                       ) ;
define('LIBVIRT_PAGER_LIMIT'      , 50                                      ) ;
