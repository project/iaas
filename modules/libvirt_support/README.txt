BASIC INFO
==========

 * This module is:
 * A experimental trial for building private cloud by using libvirt.
 * Only manage list of VMs by libvirt.
 * The purpose of this module is to make feasibility to manage multiple cloud.
 

DIRECTORY STRUCTURE
===================

aws
  +-aws (depends on REST Client module)
  +-modules (Will be called by other EC2 API compatible clouds like Eucalyptus,  OpenStack nova and etc.)
    +-aws_ec2
      - aws_ec2_api.module (depends on aws_common) 
      - aws_ec2_lib.module (depends on ec2_api)
      - << EBS Volume wrapper     >> (.inc)
      - << Elastic IP wrapper     >> (.inc)
      - << Images wrapper         >> (.inc)
      - << Instances wrapper      >> (.inc)
      - << Register Image wrapper >> (.inc)
      - << Security Group wrapper >> (.inc)
      - << Snapshot wrapper       >> (.inc)
      - << SSH Key>> (User Keys Management based on Permission) wrapper (.inc)
      - ...

iaas
  +-modules
    +-libvirt

rest_client




CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt