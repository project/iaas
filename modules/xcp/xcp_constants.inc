<?php

/**
 * @file
 * XCP Constants File.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 * 
 */

define('XCP_CLOUD_PREFIX'                  , 'cloud_'                                   ) ;
define('XCP_CLOUD_NONE'                    , '- none -'                                 ) ;
define('XCP_CONTEXT'                       , 'xcp'                                      ) ;
define('XCP_DISPLAY_NAME'                  , 'XCP'                                      ) ; // Change this variables for different cloud
define('XCP_PATH'                          , 'clouds/xcp'                               ) ;
define('XCP_MONITORING_SERVER_MANAGER_FILE', 'cloud_snmpmanager.php'                    ) ;

define('XCP_CONSOLE_TABLE'       , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_console'          ) ;
define('XCP_HOSTS_TABLE'         , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_hosts'            ) ;
define('XCP_INSTANCES_TABLE'     , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_instances'        ) ;
define('XCP_INSTANCES_LOCK_TABLE', XCP_CLOUD_PREFIX . XCP_CONTEXT . '_instances_lock'   ) ;
define('XCP_MONITOR_INFO_TABLE'  , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_monitor_info'     ) ;
define('XCP_POOLS_TABLE'         , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_pools'            ) ;
define('XCP_NETWORK_TABLE'       , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_network'          ) ;
define('XCP_PIF_TABLE'           , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_pif'              ) ;
define('XCP_VIF_TABLE'           , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_vif'              ) ;
define('XCP_VM_NETWORK_TABLE'    , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_vm_network'       ) ;
define('XCP_CONSOLE_SESSION_TABLE', XCP_CLOUD_PREFIX . XCP_CONTEXT . '_console_session' ) ;
define('XCP_QUEUE_TABLE'         , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_queue'            ) ; 
define('XCP_SNAPSHOT_TABLE'      , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_snapshot'         ) ; 
define('XCP_SNAPSHOT_SCHEDULE'   , XCP_CLOUD_PREFIX . XCP_CONTEXT . '_snapshot_schedule') ;
define('XCP_CONSOLE_SESSION_TIMEOUT', 3 ) ;
define('XCP_INFO_DOWNLOAD_INTERVAL' , 3 ) ;

// XCP Task list
define('XCP_VM_LAUNCH'           , 'vm_launch'        ) ;
define('XCP_VM_TERMINATE'        , 'vm_terminate'     ) ;
define('XCP_VM_HARD_SHUTDOWN'    , 'vm_hard_shutdown' ) ;
define('XCP_VM_HARD_REBOOT'      , 'vm_hard_reboot'   ) ;
define('XCP_VM_SUSPEND'          , 'vm_suspend'       ) ;
define('XCP_VM_RESUME'           , 'vm_resume'        ) ;
define('XCP_VM_REBOOT'           , 'vm_reboot'        ) ;
define('XCP_VM_CLONE'            , 'vm_clone'         ) ;
define('XCP_VM_DESTROY'          , 'vm_destroy'       ) ;
define('XCP_VM_INFO_DOWNLOAD'    , 'vm_info_download' ) ;
define('XCP_VM_SNAPSHOT'         , 'vm_snapshot'      ) ;
define('XCP_SS_REVERT'           , 'ss_revert'        ) ;
define('XCP_VM_NETWORK_ASSIGN'   , 'vm_network_assign') ;
define('XCP_VM_NETWORK_RELEASE'  , 'vm_network_release') ;

/*
 * XCP Task Status: The status has to be one of the following.
*/
define('XCP_TASK_STATUS_INITIATED'   , 'initiated'   ) ;
define('XCP_TASK_STATUS_IN_PROGRESS' , 'in_progress' ) ;
define('XCP_TASK_STATUS_COMPLETE'    , 'complete'    ) ;
define('XCP_TASK_STATUS_FAILED'      , 'failed'      ) ;
define('XCP_TASK_STATUS_CANCELED'    , 'canceled'    ) ;
define('XCP_TASK_STATUS_SKIPPED'     , 'skipped'     ) ;

define('XCP_MONITORING_SUBNET'       , variable_get( XCP_CONTEXT . '_monitoring_subnet', '0.0.0.0/32' ) ) ; 
define('XCP_MONITOR_URL'             , variable_get( XCP_CONTEXT . '_monitor_url'      , 'http://' ) ) ; 

define('XCP_DEFAULT_INSTANCE_TYPE'   , 'm1.small') ;
define('XCP_PAGER_LIMIT'             , 50        ) ;
