<?php

/**
 * @file
 * XCP Q DB File.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

function _xcp_add_task($XCP_TASK, $uuid, $data, $status_msg) {

  global $user ;

  $name = 'cron' ; // default value
  if ( isset($user->name) ) {

      $name = $user->name ;
  }
  
  drupal_queue_include() ;  
  $insert_query = 'INSERT INTO {' . XCP_QUEUE_TABLE . '} 
                  ( `task`,  
                    `uuid`,  
                    `data`,  
                    `status`,  
                    `status_message`,  
                    `created`, 
                    `updated`,  
                    `initiated_by_user` 
                  )  values ';
  
  $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s', '%s', CURRENT_TIMESTAMP, CURRENT_TIMESTAMP, '%s' ) " ;
  
  $query_args = array(
    $XCP_TASK                ,
    $uuid                    ,
    $data                    ,
    XCP_TASK_STATUS_INITIATED,
    $status_msg              ,
    $name                    ,
  ) ;  
  db_query( $insert_query, $query_args );
}



function _xcp_add_download_info_task($XCP_TASK, $uuid, $data, $status_msg) {

  global $user ;

  $name = 'cron' ; // default value
  if ( isset($user->name) ) {

      $name = $user->name ;
  }

  drupal_queue_include() ;

  // Check if download task already exists then update it else add new download task
  $query     = 'SELECT * FROM {' . XCP_QUEUE_TABLE . "} c WHERE `task` ='" . XCP_VM_INFO_DOWNLOAD . "' and TIMESTAMPDIFF( MINUTE, updated, CURRENT_TIMESTAMP) > " . XCP_INFO_DOWNLOAD_INTERVAL . " order by updated asc limit 1 " ;
  $db_result = db_query($query);

  $qid = FALSE ;
  
  while ($row = db_fetch_object($db_result) ) {

      $qid = $row->qid ;
  }

  if ($qid !== FALSE ) {

      _xcp_update_task_status($qid , 'Task added: Download VM Info' ,  XCP_TASK_STATUS_INITIATED ) ;
  }
  else {
  
      _xcp_add_task( XCP_VM_INFO_DOWNLOAD, '', '' , 'Task added: Download VM Info') ;
  }
}


function _xcp_get_next_task_to_exec() {

  $query = 'SELECT c.* FROM {' . XCP_QUEUE_TABLE . "} c WHERE `status` = '" . XCP_TASK_STATUS_INITIATED . "' order by created asc limit 1 " ;
  $db_result = db_query($query);    
  
  $task_to_execute = array() ;
  
  while ($row = db_fetch_object($db_result) ) {
    
    $task_to_execute['qid'              ] =  $row->qid               ;     
    $task_to_execute['task'             ] =  $row->task              ;     
    $task_to_execute['uuid'             ] =  $row->uuid              ;     
    $task_to_execute['data'             ] =  $row->data              ;     
    $task_to_execute['status'           ] =  $row->status            ;     
    $task_to_execute['status_message'   ] =  $row->status_message    ;     
    $task_to_execute['created'          ] =  $row->created           ;     
    $task_to_execute['updated'          ] =  $row->updated           ;     
    $task_to_execute['initiated_by_user'] =  $row->initiated_by_user ;     
  }
      
  return $task_to_execute ;    
}


function _xcp_get_next_task_in_progress() {

  $query = 'SELECT * , TIMESTAMPDIFF( MINUTE, updated, CURRENT_TIMESTAMP) diff_in_min FROM {' . XCP_QUEUE_TABLE . "} c WHERE `status` ='" . XCP_TASK_STATUS_IN_PROGRESS . "' order by created asc limit 1 " ;
  $db_result = db_query($query);    
  
  $task_to_execute = array() ;
  
  while ($row = db_fetch_object($db_result) ) {
    
    $task_to_execute['qid'              ] =  $row->qid               ;     
    $task_to_execute['task'             ] =  $row->task              ;     
    $task_to_execute['uuid'             ] =  $row->uuid              ;     
    $task_to_execute['data'             ] =  $row->data              ;     
    $task_to_execute['status'           ] =  $row->status            ;     
    $task_to_execute['status_message'   ] =  $row->status_message    ;     
    $task_to_execute['created'          ] =  $row->created           ;     
    $task_to_execute['updated'          ] =  $row->updated           ;     
    $task_to_execute['initiated_by_user'] =  $row->initiated_by_user ;     
    $task_to_execute['time_running'     ] =  $row->diff_in_min       ;
  }
      
  return $task_to_execute ;    
}


function _xcp_update_task_status($qid, $status_msg, $XCP_Task_Status) {

  $update_query = 'update {' . XCP_QUEUE_TABLE . "} c set `status`='%s', `status_message`='%s', `updated`=CURRENT_TIMESTAMP  where `qid`='%s' " ;
  $query_args = array(
    $XCP_Task_Status,
    $status_msg     ,
    $qid            ,
  ) ;
  db_query( $update_query, $query_args );
}



function _xcp_get_task_list() {

  global $user;
  if (isset($user->name) === FALSE ) {

      return array() ;
  }
  
  $owner = $user->name ;
  
  if (user_access('administer xcp') ) {
    $query = 'SELECT c.* FROM {' . XCP_QUEUE_TABLE . "} c WHERE `initiated_by_user` != '' and task != '".  XCP_VM_INFO_DOWNLOAD  . "' order by created desc limit 5 " ;
  }
  else {
    $query = 'SELECT c.* FROM {' . XCP_QUEUE_TABLE . "} c WHERE `initiated_by_user` = '" . $owner . "' and task != '".  XCP_VM_INFO_DOWNLOAD  . "' order by created desc limit 5 " ;
  }
  
  $db_result = db_query($query);    
  
  $task_to_execute = array() ;
  $task_list = array() ;
  
  while ($row = db_fetch_object($db_result) ) {
    
    $task_to_execute['qid'              ] =  $row->qid               ;     
    $task_to_execute['task'             ] =  $row->task              ;     
    $task_to_execute['uuid'             ] =  $row->uuid              ;     
    $task_to_execute['data'             ] =  $row->data              ;     
    $task_to_execute['status'           ] =  $row->status            ;     
    $task_to_execute['status_message'   ] =  $row->status_message    ;     
    $task_to_execute['created'          ] =  $row->created           ;     
    $task_to_execute['updated'          ] =  $row->updated           ;     
    $task_to_execute['initiated_by_user'] =  $row->initiated_by_user ;     
    $task_list[] = $task_to_execute ;
  }
      
  return $task_list ;    
}


function _xcp_get_all_queued_task_list() {

  global $user;
  $owner = $user->name ;
  
  $query = 'SELECT c.* FROM {' . XCP_QUEUE_TABLE . "} c WHERE `status`  in ( '" . XCP_TASK_STATUS_INITIATED . "', '" . XCP_TASK_STATUS_IN_PROGRESS .  "' ) order by created desc " ;
  $db_result = db_query($query);    
  
  $task_to_execute = array() ;
  $task_list = array() ;
  
  while ($row = db_fetch_object($db_result) ) {
    
    $task_to_execute['qid'              ] =  $row->qid               ;     
    $task_to_execute['task'             ] =  $row->task              ;     
    $task_to_execute['uuid'             ] =  $row->uuid              ;     
    $task_to_execute['data'             ] =  $row->data              ;     
    $task_to_execute['status'           ] =  $row->status            ;     
    $task_to_execute['status_message'   ] =  $row->status_message    ;     
    $task_to_execute['created'          ] =  $row->created           ;     
    $task_to_execute['updated'          ] =  $row->updated           ;     
    $task_to_execute['initiated_by_user'] =  $row->initiated_by_user ;     

    if (empty($row->uuid) === FALSE ) {

      $task_list[$row->uuid] = $task_to_execute ;
    }  
  }
      
  return $task_list ;    
}



function _xcp_get_next_task_by_vm($uuid, $task_id) {

  $query = 'SELECT c.* FROM {' . XCP_QUEUE_TABLE . "} c WHERE uuid='%s' and task='%s' order by created desc limit 1 " ;
  $query_args = array() ;
  $query_args[]  = $uuid ;
  $query_args[]  = $task_id ;
  
  $db_result = db_query($query , $query_args );

  $task_to_execute = array() ;

  while ($row = db_fetch_object($db_result) ) {

    $task_to_execute['qid'              ] =  $row->qid               ;
    $task_to_execute['task'             ] =  $row->task              ;
    $task_to_execute['uuid'             ] =  $row->uuid              ;
    $task_to_execute['data'             ] =  $row->data              ;
    $task_to_execute['status'           ] =  $row->status            ;
    $task_to_execute['status_message'   ] =  $row->status_message    ;
    $task_to_execute['created'          ] =  $row->created           ;
    $task_to_execute['updated'          ] =  $row->updated           ;
    $task_to_execute['initiated_by_user'] =  $row->initiated_by_user ;
  }

  return $task_to_execute ;
}
