// $Id$

/**
 * @file
 * Used for this module
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

if (Drupal.jsEnabled) {
  $(document).ready(function () {
  
  setTimeout("callbackXCP()", 1000);
  });
}  


function updateImageParam(img_name,  cnt)
{
  if (img_name == null )
    return ;
    
  img = document.getElementById(img_name ) ;
  if (img == null )
    return ;
  
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
}


function callbackXCP() {
  
    
  var img = document.getElementById('xcp_cpu0') ;
  var src = img.src ;
  if (src.indexOf('graph_error.png') != -1 )
  {
    // Display only one Image not Found Errror
    for (index_cnt=7;index_cnt<=8;index_cnt++)
    {  
      img_div = document.getElementById('div_network_traffic_'+index_cnt ) ;
      img_div.innerHTML = "" ;
    }
    return ;
  }
    

  var index = src.search(/TmpParam/) ;
  
  var res = src.split(';TmpParam=') ; 
  var cnt = ( res[1] * 1 ) + 1 ;
  if ( cnt >= 32768 ) 
    cnt = 0 ;
    
  //img.src = res[0] + ";TmpParam=" + cnt ;
  updateImageParam('xcp_cpu0'                , cnt ) ;
  updateImageParam('xcp_memory_internal_free', cnt ) ;
  updateImageParam('xcp_memory_target'       , cnt ) ;
  updateImageParam('xcp_vbd_hda_read'        , cnt ) ;
  updateImageParam('xcp_vbd_hda_write'       , cnt ) ;
  updateImageParam('xcp_vif_1_rx'            , cnt ) ;
  updateImageParam('xcp_vif_1_tx'            , cnt ) ;
  
  /*for (index_cnt=7;index_cnt<=8;index_cnt++)
  {
    img = document.getElementById('network_traffic_'+index_cnt ) ;
    res = img.src.split(';TmpParam=') ;
    img.src = res[0] + ";TmpParam="+ cnt ;
  }

  img = document.getElementById('if_packets') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  
  img = document.getElementById('if_octets') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  
  img = document.getElementById('if_errors') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  
  img = document.getElementById('if_dropped') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  
  img = document.getElementById('disk_ops') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  
  img = document.getElementById('disk_octets') ;
  res = img.src.split(';TmpParam=') ;
  img.src = res[0] + ";TmpParam="+ cnt ;
  */
  
  var beginArr = res[0].split(';begin=-') ; 
  var begin = beginArr[1] ;
  var timeoutPeriod = begin/600 ;

  if ( timeoutPeriod > 5 )
    timeoutPeriod = 5 ;
  else
    timeoutPeriod = 3 ;

  setTimeout("callbackXCP()", timeoutPeriod* 1000);
}