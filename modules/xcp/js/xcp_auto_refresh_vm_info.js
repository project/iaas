// $Id$

/**
 * @file
 * Used for this module
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/16
 */

var refresh_in_sec = 10 ;
var autoupdate_xcp_button_list_id ;  

if ( Drupal.jsEnabled ) {
  $(document).ready(function() {
    autoupdate_xcp_button_list_id = setInterval('autoupdate_xcp_button_list()', refresh_in_sec * 1000);
 });
}
  

function autoupdate_xcp_button_list() {

  var vm_id = $('input[name=vm_id]'         ).attr('value') ;
  var $url  = $('input[name=autoupdate_url]').attr('value') ;
    
  $.get(
      $url,
      { 'vm_id' : vm_id, }, 
      update_xcp_button_list
    );
}


function update_xcp_button_list(response) {

  var result = Drupal.parseJson(response);
  var curr_state = $('input[name=vm_current_state]' ).attr('value') ;
  var lock_state = $('select[name=lock_text]' ).val() ;
  
  if (result.html == 'NULL' && curr_state == 'IN_PROGRESS' ) { // Returned empty string. Operation not in progress
  
    location.reload();
    return ;  // Done reload    
  }  
  else if ( curr_state != 'IN_PROGRESS' && result.html != 'NULL' ) { // Operation in progress
  
    location.reload();
    return ;  // Done reload        
  }
  else if (lock_state != result.lock ) {
    
    location.reload();
    return ;  // Done reload        
  }
}

