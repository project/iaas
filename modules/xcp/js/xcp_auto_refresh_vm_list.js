// $Id$

/**
 * @file
 * Used for this module
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


var refresh_in_sec = 10 ;
 
if ( Drupal.jsEnabled ) {
  $(document).ready(function() {
    setInterval("autoupdate_xcp_list()", refresh_in_sec * 1000);
 });
 }
  

function autoupdate_xcp_list() {
    
  var sort_col_val = $("img[alt$='sort icon']").parent().text() ;
  order = sort_col_val.substr( 0 , sort_col_val.length/2 ) ;
  var sort_img_src = $("img[alt$='sort icon']").attr('src') ;
  var sort = 'desc' ;
  if ( sort_img_src != '' && sort_img_src.indexOf('desc') != -1 ) {
  
    sort = 'asc' ;
  }
  
  var tgt_url = $("img[alt$='sort icon']").parent().attr('href') ;
  var opr_index = tgt_url.indexOf('operation=')  ; 
  var operation = '' ;
  var filter    = '' ;
  
  if ( opr_index != - 1) {
      
    operation = tgt_url.charAt( opr_index +  'operation='.length ) ;
    // Operation exists check filter value
    var fltr_start_index = tgt_url.indexOf('filter=')  ; 
    var fltr_end_index   = tgt_url.indexOf('&' , fltr_start_index )  ; 
    
    if ( fltr_start_index != -1 && fltr_end_index != -1 ) {
      filter = tgt_url.substring( fltr_start_index + 'filter='.length , fltr_end_index ) ;
      filter = unescape( filter) ; 
    } 
  }  
  
  
  var page = 0 ;
  var page_start_index = tgt_url.indexOf('page=')  ; 
  if ( page_start_index != - 1) {
      
    var page_end_index   = tgt_url.indexOf('&' , page_start_index )  ; 
    if ( page_end_index == -1 )  {
    
      page_end_index = tgt_url.length  ;
    }
    
    page = tgt_url.substring( page_start_index + 'page='.length , page_end_index ) ;
  }
  
  $url = $('#xcp_list_table').attr('autoupdate_url') ;
  $.get($url , { 'order'          : order, 
                 'sort'           : sort,
                 'operation'      : operation,
                 'filter'         : filter,
                 'page'           : page, 
               }, 
        update_xcp_list );
   

 }
 
 
function update_xcp_list(response) {

  var result = Drupal.parseJson(response);
  
  var head_present = $('#xcp_list_table').children('thead').length ;
  var body_present = $('#xcp_list_table').children('tbody').length ;
  
  if (result.html != 'NULL' ) { // Returned empty string. skip the output
  
    if (head_present) { // Head already present
        
      if (body_present) {
          
        $('#xcp_list_table tbody').replaceWith(result.html);    
      }
      else { // Body not present
      
        $('#xcp_list_table').append(result.html);    
      }
    } 
    else {
    
      var head_str = $('#xcp_list_table').html() ;
      head_str = head_str.replace( '<tbody>' , '<thead>' ) ; 
      head_str = head_str.replace( '</tbody>' , '</thead>' ) ;
      
      $('#xcp_list_table tbody').replaceWith(head_str); 
      $('#xcp_list_table tbody').append(result.html); 
    }    
    
    Drupal.behaviors.showActionButtons() ;
  }
  else {
  
    if (head_present && body_present) {
    
      $('#xcp_list_table tbody').remove() ;    
    }
  }
}




