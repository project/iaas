<?php

/**
 * @file
 * XCP Tasks will be executed
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Add a Task to XCp queue
 * 
 * @param $item
 *          Task to be added to the Queue
 */
function _xcp_add_item_to_queue($item) {
  $xcp_queue = DrupalQueue::get('xcp_queue');
  $xcp_queue->createItem($item) ;
}  


/**
 * Add Launch VM task in Queue
 *
 * @param $uuid
 *            Id of VM to be launched 
 * @param $name 
 */
function _xcp_add_launch_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_LAUNCH,
                 $uuid, 'name=' . $name, "Task added: Launch $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_LAUNCH ) ;
}


/**
 * Add Launch VM task in Queue
 * In this case the VM should be launched on a particular Host
 *
 * @param $uuid
 *          Id of VM to be launched
 * @param $host_ref
 *          Host on which the VM is to be launched
 * @param $name 
 */
function _xcp_add_launch_vm_on_host_task($uuid, $host_ref, $name) {
  _xcp_add_task( XCP_VM_LAUNCH,
                 $uuid, 'host=' . $host_ref . ", name=" . $name, "Task added: Launch $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_LAUNCH ) ;
}


/**
 * Add shutdown VM task to the Queue
 * 
 * @param $uuid
 *          Id of VM
 * @param $name 
 */
function _xcp_add_shutdown_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_TERMINATE,
                 $uuid, 'name=' . $name, "Task added: Terminate $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_TERMINATE ) ;
}


/**
 * Add Hard Shutdown VM task to the Queue
 * @param $uuid
 *          Id of VM
 * @param $name 
 */
function _xcp_add_hard_shutdown_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_HARD_SHUTDOWN,
                 $uuid, 'name=' . $name, "Task added: Hard Shutdown $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_HARD_SHUTDOWN ) ;
}


/**
 * Add Hard Reboot task to the Queue
 *
 * @param $uuid
 *          Id of VM to be rebooted
 * @param $name 
 */
function _xcp_add_hard_reboot_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_HARD_REBOOT,
                 $uuid, 'name=' . $name, "Task added: Hard Reboot $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_HARD_REBOOT ) ;
}

/**
 * Add reboot task to the Queue
 * 
 * @param $uuid
 *          Id of VM to be rebooted
 * @param $name 
 */
function _xcp_add_reboot_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_REBOOT,
                 $uuid, 'name=' . $name, "Task added: Reboot $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_REBOOT ) ;
}


/**
 * Add Suspend task to the Queue
 * 
 * @param $uuid
 *          Id of VM to be rebooted
 * @param $name 
 */
function _xcp_add_suspend_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_SUSPEND,
                 $uuid, 'name=' . $name, "Task added: Suspend $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_SUSPEND ) ;
}


/**
 * Add Resume task to the Queue
 * 
 * @param $uuid
 *          Id of VM 
 * @param $name 
 */
function _xcp_add_resume_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_RESUME,
                 $uuid, 'name=' . $name, "Task added: Resume $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_RESUME ) ;
}


/**
 *  Download XCP Information from the Master Host
 *  Save this information in the datatbase
 */
function _xcp_add_download_vm_info_task() {
 // _xcp_add_task( XCP_VM_INFO_DOWNLOAD,
 //                '', '' , 'Task added: Download VM Info') ;
  _xcp_add_download_info_task(XCP_VM_INFO_DOWNLOAD,
                 '', '' , 'Task added: Download VM Info') ;
  _xcp_add_item_to_queue( XCP_VM_INFO_DOWNLOAD ) ;
}


/**
 * Add Revert task to the Queue
 *
 * @param $uuid
 *          Id of VM 
 * @param $name 
 */
function _xcp_add_snapshot_revert_task($uuid, $name) {
  _xcp_add_task( XCP_SS_REVERT,
                 $uuid, 'name=' . $name, "Task added: Snapshot revert $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_SS_REVERT ) ;
}


/**
 * Add Clone VM task to the Queue
 *
 * @param $uuid
 *          Id of master vm to be cloned 
 * @param $name 
 */
function _xcp_add_clone_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_CLONE,
                 $uuid, 'name=' . $name, "Task added: Cloning for $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_CLONE ) ;
}

/**
 * Add Destroy VM task to the Queue
 *
 * @param $uuid
 *          Id of VM to be destroyed
 * @param $name 
 */
function _xcp_add_destroy_vm_task($uuid, $name) {
  _xcp_add_task( XCP_VM_DESTROY,
                 $uuid, 'name=' . $name, "Task added: Destroy $name. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_DESTROY ) ;
}

function _xcp_add_snapshot_vm_task($uuid) {
  _xcp_add_task( XCP_VM_SNAPSHOT,
                 $uuid, '' ,  "Task added: Snapshot $uuid. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_SNAPSHOT ) ;
}


function _xcp_add_assign_network_vm_task($uuid, $network_id) {
  _xcp_add_task( XCP_VM_NETWORK_ASSIGN,
                 $uuid, $network_id ,  "Task added: Assign network to $uuid. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_NETWORK_ASSIGN ) ;
}


function _xcp_release_network_vm_task($uuid, $vif_id) {
  _xcp_add_task( XCP_VM_NETWORK_RELEASE,
                 $uuid , $vif_id,   "Task added: Release network $vif_id. This may require some time." ) ;
  _xcp_add_item_to_queue( XCP_VM_NETWORK_RELEASE ) ;
}
