<?php

/**
 * @file
 * XCP DB File.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

/**
 * Updated by yas   2011/02/17
 * Updated by yas   2011/02/16
 */

function _xcp_get_describe_instances_query() {

  global $user;
  $owner = $user->name ;
  return $query = "SELECT c.*, if (vm_state='Suspended', 'Pending', vm_state) as statename FROM {" . XCP_INSTANCES_TABLE . "} c WHERE assigned_to='$owner'  and %s like '%%%s%%' " ;
}


function _xcp_get_all_running_vms() {

  $query     = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE `vm_state` = 'Running' and vm_ip <> '' " ;
  $db_result = db_query($query);
  $os_str    = '' ;
  $dns_names = '' ;

  while ($row = db_fetch_object($db_result)) {
    $dns_names .=  $row->vm_ip     . ', ' ;
    $os_str    .=  $row->os_distro . ', ' ;
  }

  if (empty($dns_names) )
    return ;
  
  $dns_names = substr($dns_names, 0, -1) ;
  $os_str    = substr($os_str   , 0, -1) ;

  $arr = array() ;
  $arr[0] = $dns_names ;
  $arr[1] = $os_str    ;

  return $arr;
}


function _xcp_get_all_instances_db($filter=array() ) {

  $zone_options = _xcp_get_pool_options() ;
  $zone         = '';

  foreach ($zone_options as $zone_opt ) {
  
    $zone = $zone_opt ;
  }
  
  $where_sql = '' ;
  
  $query_args = array() ;
  
  // Check if Filter is present
  if ( isset($filter['filter_value'] ) && !empty($filter['filter_value']) ) {
  
    $column = $filter['column'] ;
    $column = $column == 'instance_id'       ? 'vmid'   : $column ;
    $column = $column == 'dns_name'          ? 'vm_ip'  : $column ;
    $column = $column == 'instance_nickname' ? 'vm_name': $column ;
    $where_sql    = " and $column like '%%%s%%' " ;
    $query_args[] = $filter['filter_value']       ;
  }
  
  if ( isset($filter['filter_state'] )&& !empty($filter['filter_state'])  ) {

    if ( $filter['filter_state'] === 'all' ) { // Get all instances
    
      $query = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE `vm_state` like '%%' " . $where_sql ;
    }
    else {

      $query = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE `vm_state` = 'Running' and vm_ip <> '' " . $where_sql ;
    }
  }
  else {

    $query = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE `vm_state` = 'Running' " . $where_sql ;
  }
  
  $db_result = db_query($query , $query_args );
  $os_str    = '' ;
  $instances_list = array() ;

  while ($instance = db_fetch_object($db_result)) {
        
    $instances_list[$instance->vmid] = array() ;
    $instances_list[$instance->vmid]['instance_id'  ] = $instance->vmid        ;
    $instances_list[$instance->vmid]['name'         ] = $instance->vm_name     ;
    $instances_list[$instance->vmid]['hostname'     ] = $instance->vm_ip       ;
    $instances_list[$instance->vmid]['state'        ] = $instance->vm_state    ;
    $instances_list[$instance->vmid]['zone'         ] = $zone                  ;
    $instances_list[$instance->vmid]['user'         ] = $instance->assigned_to ;
    $instances_list[$instance->vmid]['runtime'      ] = $instance->launchtime  ;
    $instances_list[$instance->vmid]['lock_status'  ] = _xcp_get_lock_status($instance->vmid) ;
    $instances_list[$instance->vmid]['cloud_context'] = XCP_CONTEXT            ;
  }
    
  return $instances_list ;
}


function _xcp_get_all_running_vm_id_ip() {

  $query     =  'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE `vm_state` = 'Running' and vm_ip <> '' " ;
  $db_result = db_query($query);
  $vm_str    = '' ;

  while ($row = db_fetch_object($db_result)) {
    $vm_str .= $row->vmid . ':' . $row->vm_ip . ', ';
  }

  $vm_str = substr($vm_str,  0,  -1);
  return $vm_str ;
}


function _xcp_get_instance_by_id_query() {

  return $query =  'SELECT c.*, e.hostname as hostname FROM {' . XCP_INSTANCES_TABLE . "} c 
            left join {" . XCP_HOSTS_TABLE . "} e on e.host_ip=c.host_ip 
            WHERE vmid='%s'" ;
}


function _xcp_get_describe_all_instances_query() {

  return $query = "SELECT c.*, if (vm_state='Suspended', 'Pending', vm_state) as statename FROM {" . XCP_INSTANCES_TABLE . "} c WHERE %s like '%%%s%%' " ;
}


function _xcp_get_describe_all_instances_query_manual_sort() {

  return $query = "SELECT * FROM ( SELECT c.*, if (vm_state='Suspended', 'Pending', vm_state) as statename , host.hostname as hostname FROM {" . XCP_INSTANCES_TABLE . "} c  
                        LEFT JOIN {" . XCP_HOSTS_TABLE . "} host on  host.host_ip=c.host_ip 
                        WHERE %s like '%%%s%%'
                    ) vm_list
                    order by statename desc, hostname , vm_name, assigned_to, launchtime " ;
}


function _xcp_get_instance_info() {

  return $query = 'SELECT * FROM {' . XCP_INSTANCES_TABLE . "} WHERE %s = '%s' " ;
}


function _xcp_get_all_halted_instances_query() {

  return $query = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE vm_state='Halted' order by vm_name " ;
}


function _xcp_get_console_info_query() {

  $query   = 'SELECT c.* from {' . XCP_CONSOLE_TABLE . '} c ' ;
  $results = db_query( $query ,   NULL );

  $result_arr = array() ;
  while ($console = db_fetch_object($results)) {

    $result_arr[$console->VM ] = array() ; // Return result as per VM array
    $result_arr[$console->VM ]['uuid']     = $console->uuid ;
    $result_arr[$console->VM ]['protocol'] = $console->protocol ;
    $result_arr[$console->VM ]['location'] = $console->location ;
    $result_arr[$console->VM ]['VM']       = $console->VM ;
  }

  return $result_arr ;
}


function _xcp_update_state_by_uuid($uuid, $state) {

  $update_query = 'UPDATE {' . XCP_INSTANCES_TABLE . "} c SET `vm_state`='%s' WHERE `vmid`='%s'" ;
  $query_args = array(
    $state ,
    $uuid  ,
  ) ;
  db_query( $update_query, $query_args );
}


function _xcp_update_name_by_uuid($uuid, $name) {

  $update_query = 'UPDATE {' . XCP_INSTANCES_TABLE . "} c SET `vm_name`='%s' WHERE `vmid`='%s'" ;
  $query_args   = array(
    $name ,
    $uuid ,
  ) ;
  db_query( $update_query, $query_args );
}


function _xcp_download_network_info_xcp() {
  $params =  _xcp_get_network_info() ;
  $size = count($params) ;
  if ( $size == 0 )
    return ;

  $insert_query = 'INSERT INTO {' . XCP_NETWORK_TABLE . '}
                  ( `uuid`,
                    `name_label`,
                    `name_description`,
                    `bridge` ,
                    `opaquereference`
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    $insert_query = $insert_query . "  ( '%s', '%s', '%s', '%s' , '%s' )   " ;
  }

  if ( $count == 0 )
    return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query('truncate table {' . XCP_NETWORK_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );
}



function _xcp_get_next_device($vm_ref) {

  $device     = 1 ;
  $query      = 'SELECT max(device) as device FROM {' . XCP_VIF_TABLE . "} c where vm_ref='%s'  group by vm_ref " ;
  $query_args = array(
    $vm_ref ,
  ) ;
  
  $db_result = db_query($query , $query_args );
  
  while ($row = db_fetch_object($db_result)) {
    $device = $row->device + 1 ;
  }

  return $device ;
}


function _xcp_get_vm_network_list() {

  $xcp_vm_network_query = "select c.*  from {" . XCP_VM_NETWORK_TABLE . "} c " ;
  $query_args = array() ;
  
  $result = db_query( $xcp_vm_network_query , $query_args );

  $vm_network_list = array() ;
  while ($key = db_fetch_object($result)) {

    if ( isset($vm_network_list[$key->vm_uuid]) === FALSE ) {

        $vm_network_list[$key->vm_uuid] = array() ;
    }

    $vm_network_list[$key->vm_uuid][$key->device] = $key->ip ;
  }

  return $vm_network_list ;
}

function _xcp_get_vif_list($vm_ref, $sort_by='') {
  
  $xcp_vif_query = 'SELECT c.* , d.bridge from {' . XCP_VIF_TABLE . '} c LEFT JOIN {' . XCP_NETWORK_TABLE . "} d on d.opaquereference=c.network where c.vm_ref='%s' " ;

  $query_args = array(
    $vm_ref ,
  ) ;
  
  if ( empty($sort_by) === FALSE ) {
  
    $xcp_vif_query .= tablesort_sql( $sort_by ) ;
  }
  
  $result = db_query( $xcp_vif_query , $query_args );

  $vif_list = array() ;
  while ($key = db_fetch_object($result)) {

    $vif_list[$key->uuid]                             = array() ;
    $vif_list[$key->uuid]['uuid'      ]               = $key->uuid ;
    $vif_list[$key->uuid]['device'    ]               = $key->device ;
    $vif_list[$key->uuid]['mac'       ]               = $key->mac ;
    $vif_list[$key->uuid]['network'   ]               = $key->bridge ;
    $vif_list[$key->uuid]['currently_attached']       = $key->currently_attached ;
    $vif_list[$key->uuid]['vif_ref']                  = $key->vif_ref ;
  }

  return $vif_list ;
}


function _xcp_download_vif_info_xcp() {

  $params = _xcp_get_vif_info() ;
  $size   = count($params) ;
  if ( $size == 0 )
    return ;

  $insert_query = 'INSERT INTO {' . XCP_VIF_TABLE . '}
                  ( `vif_ref`   ,
                    `uuid`      ,
                    `device`    ,
                    `network`   ,
                    `vm_ref`    ,
                    `mac`       ,
                    `currently_attached`
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= '  , ' ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    $insert_query = $insert_query . "  ( '%s', '%s', '%s', '%s' , '%s', '%s', '%s' )   " ;
  }

  if ( $count == 0 )
    return TRUE;

  db_query('truncate table {' . XCP_VIF_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );
}


function _xcp_download_pif_info_xcp() {
    
  $params = _xcp_get_pif_info() ;
  $size   = count($params) ;
  if ( $size == 0 )
    return ;

  $insert_query = 'INSERT INTO {' . XCP_PIF_TABLE . '}
                  ( `uuid`                ,
                    `device`              ,
                    `mac`                 ,
                    `physical`            ,
                    `currently_attached`  ,
                    `mtu`                 ,
                    `bond_master_of`      ,
                    `bond_slave_of`       ,
                    `network_uuid`        ,
                    `host_uuid`           ,
                    `opaquereference`     
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= '  , ' ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    $insert_query = $insert_query . "  ( '%s', '%s', '%s', '%s' , '%s', '%s', '%s' , '%s', '%s', '%s', '%s' )   " ;
  }

  if ( $count == 0 )
    return TRUE;

  db_query('truncate table {' . XCP_PIF_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );
}


function _xcp_download_vm_network_info_xcp() {

  $params =  _xcp_get_vm_network_info() ;
  $size = count($params) ;
  if ( $size == 0 )
    return ;

  $insert_query = 'INSERT INTO {' . XCP_VM_NETWORK_TABLE . '}
                  ( `vm_uuid`   ,
                    `device`    ,
                    `ip`        
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= '  , ' ;
    }

    $query_args[] = $params[$count++] ;
    $device       = $params[$count++] ;
    $device       = substr( $device , 0 , strpos( $device , '/ip') ) ;
    $query_args[] = $device ;
    $query_args[] = $params[$count++];
    
    $insert_query = $insert_query . "  ( '%s', '%s', '%s' )   " ;
  }

  if ( $count == 0 )
    return TRUE;

  db_query('truncate table {' . XCP_VM_NETWORK_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );
}


function _xcp_download_pool_info_xcp() {

  $params =  _xcp_get_pool_info() ;
  $size = count($params) ;
  if ( $size == 0 )
  return ;

  $insert_query = 'INSERT INTO {' . XCP_POOLS_TABLE . '} 
                  ( `uuid`,
                   `poolname`,  
                    `master`
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++];

    $insert_query = $insert_query . "  (  '%s', '%s', '%s' )   " ;
  }

  if ( $count == 0 )
  return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query('truncate table {' . XCP_POOLS_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );
}


function _xcp_download_instances_info_xcp($dont_update_collectd='') {

  $params = _xcp_get_info() ;
  $size   = count($params) ;
  if ( $size == 0 )
    return ;

  $id_user_arr = cloud_get_all_nickname('vmid', 'assigned_to', XCP_INSTANCES_TABLE) ;
  $already_existing_dns = _xcp_get_all_dns();
  //print 'here';die;
  $insert_query = 'INSERT INTO {' . XCP_INSTANCES_TABLE . '} 
                  ( `vmid`,  
                    `vm_name`,  
                    `vm_ip`,  
                    `host_ip`,  
                    `vm_state`,  
                    `assigned_to`, 
                    `opaquereference`,  
                    `os_distro`, 
                    `launchtime`,
                    `vcpu_number` 
                  )  values '; 

  $count = 0 ;
  $query_args = array() ;

  $dns_from_xcp= array();
  while ($count < $size ) {
    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $uuid = $params[$count] ;
    $query_args[]   = $params[$count++] ;
    $query_args[]   = $params[$count++] ;
    $dns_from_xcp[] = $params[$count  ] ;
    $query_args[]   = $params[$count++] ;
    $query_args[]   = $params[$count++] ;
    $instance_state = $query_args[] = $params[$count++] ;

    $tmp_user_name  = cloud_check_in_array( $id_user_arr ,  trim($uuid) ) ;
    $query_args[]   = $tmp_user_name != NULL ? $tmp_user_name : '' ;

    $query_args[]   = $params[$count++] ;
    $query_args[]   = $params[$count++] ;
    $launch_time    = $query_args[] = $params[$count++] ;
    $query_args[]   = $params[$count++] ;

    $insert_query = $insert_query . "  ( '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s' )  " ;

    if ( $instance_state === 'Halted' ) {
    
            $instance_state = 'terminated' ; // The common state for terminated is 'terminated' not halted
    }
    
    cloud_billing_update(XCP_CONTEXT,
                         $uuid,
                         $instance_state,
                         $launch_time,
                         XCP_DEFAULT_INSTANCE_TYPE
                        );
  }

  if ( $count == 0 )  return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query( 'truncate table {' . XCP_INSTANCES_TABLE . '}',  NULL );
  db_query( $insert_query, $query_args );

  $snmp_last_update_diff_in_min  = cloud_get_last_update_snmp_timediff(XCP_CONTEXT);
  $new_instances = array_diff($dns_from_xcp, $already_existing_dns);

  if ($snmp_last_update_diff_in_min > 0 || count($new_instances) >  0 ) {

    $arr_to_send                = _xcp_get_all_running_vms() ;
    $collectd_snmp_host_entries = $arr_to_send[0] ;
    $osstr                      = $arr_to_send[1] ;

    if (!empty($collectd_snmp_host_entries) ) {

      $vm_str       = _xcp_get_all_running_vm_id_ip() ;
      $xcp_userID   = variable_get(XCP_CONTEXT . '_user_id'      ,  '' ) ;
      $xcp_password = variable_get(XCP_CONTEXT . '_user_password', '' ) ;

      $host_arr = _xcp_get_host_cpu_info_query() ;
      $host_str = '' ;
      foreach ($host_arr as $host) {
        $host_str  .= $host['ip'] . ", " ;
      }
      $host_str   = substr($host_str, 0, -1);

      // Update the master host_ip
      $url      = variable_get('xcp_host_uri', '' ) ;
      $url      = parse_url($url);
      $xcp_host = $url['host'];
      $master   = $xcp_host ;

      _xcp_update_snmp_host_entries_with_os('all', $collectd_snmp_host_entries, $osstr , $host_str , $vm_str , $master, $xcp_userID , $xcp_password ) ;
      cloud_update_host_entries_last_update_time(XCP_CONTEXT);
      cloud_log_to_db('SNMP_HOST_ENTRIES', $collectd_snmp_host_entries . ': Time diff=' . $snmp_last_update_diff_in_min . '::Count=' . count($new_instances) );
    }

    // Download following information only after specific time or event
    _xcp_download_console_info_xcp()    ;
    _xcp_download_host_info_xcp()       ;
    _xcp_download_snapshot_info_xcp()   ;
    _xcp_download_pool_info_xcp()       ;
    _xcp_download_network_info_xcp()    ;
    _xcp_download_pif_info_xcp()        ;
    _xcp_download_vif_info_xcp()        ;
    _xcp_download_vm_network_info_xcp() ;
  }

  return TRUE;
}


function _xcp_download_host_info_xcp() {

  $params = _xcp_get_host_info() ;

  $size = count($params) ;
  if ( $size == 0 )
    return ;

  db_query( 'truncate table {' . XCP_HOSTS_TABLE . '}',  NULL );

  $insert_query = 'INSERT INTO {' . XCP_HOSTS_TABLE . '}
                  ( `hostid` ,  
                    `hostname` ,  
                    `host_ip` ,  
                    `no_of_cpu` ,  
                    `opaquereference` 
                  )  values '; 

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s', '%s'  )   " ;
  }

  if ( $count == 0 )
    return TRUE;


  $insert_query = substr($insert_query,  0,  -1);
  db_query( $insert_query, $query_args );

  return TRUE;
}


function _xcp_download_snapshot_info_xcp() {

  $params =  _xcp_get_snapshot_info() ;

  $size = count($params) ;
  if ( $size == 0 )
    return ;

  $insert_query = 'INSERT INTO {' . XCP_SNAPSHOT_TABLE . '}
                  ( `uuid`,  
                    `name`,  
                    `snapshot_of`,  
                    `snapshot_time`
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    // Date
    $create_tmp_time   = date_parse($params[$count++] ) ;
    $gm_launch_time   = gmmktime( $create_tmp_time['hour'] , $create_tmp_time['minute'] , $create_tmp_time['second'] , $create_tmp_time['month'], $create_tmp_time['day'], $create_tmp_time['year'] ) ;
    $query_args[] = $gm_launch_time ;
    $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s' )  " ;
  }

  db_query('truncate table {' . XCP_SNAPSHOT_TABLE . '}', NULL );

  if ( $count == 0 )
    return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query( $insert_query, $query_args );

  return TRUE;
}


function _xcp_download_console_info_xcp() {

  $params =  _xcp_get_xcp_console_info() ;

  $size = count($params) ;
  if ( $size == 0 )
  return ;

  db_query( 'truncate table {' . XCP_CONSOLE_TABLE . '}',  NULL );

  $insert_query = 'INSERT INTO {' . XCP_CONSOLE_TABLE . '}
                  ( `uuid` ,  
                    `protocol` ,  
                    `location` ,  
                    `VM`  
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  while ($count < $size ) {

    if ($count > 0 ) {

      $insert_query  .= " , " ;
    }

    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;
    $query_args[] = $params[$count++] ;

    $insert_query = $insert_query . "  (  '%s', '%s', '%s', '%s' )  " ;

  }

  if ( $count == 0 )
    return TRUE;

  $insert_query = substr($insert_query,  0,  -1);
  db_query( $insert_query, $query_args );
  return TRUE;
}


/*
 * This function returns the number of VMs running on the hosts.
 * This function can be used for provisioning.
 * 
 */
function _xcp_get_vm_count_by_host() {

  $query      =  'SELECT count(*) as vm_cnt, `host_ip` FROM {' . XCP_INSTANCES_TABLE . "} where `vm_state` = 'Running' group by `host_ip` " ;
  $query_args = array() ;

  $results = db_query( $query, $query_args );

  $host_vm_cnt_arr = array() ;
  while ($host_cnt = db_fetch_object($results)) {

    $host_vm_cnt_arr[$host_cnt->host_ip] = $host_cnt->vm_cnt ;
  }

  return $host_vm_cnt_arr ;
}


function _xcp_get_all_host_query() {

  $query = 'SELECT *  FROM {' . XCP_HOSTS_TABLE . '}' ;

  return $query ;
}


function _xcp_get_host_info_db() {

  $query = 'SELECT c.* FROM {' . XCP_HOSTS_TABLE . '} c ' ;

  $results = db_query( $query ,   NULL );

  $result_arr = array() ;
  while ($host = db_fetch_object($results)) {

    $result_arr[$host->hostid]              = array() ;
    $result_arr[$host->hostid]['ip'       ] = $host->host_ip ;
    $result_arr[$host->hostid]['no_of_cpu'] = $host->no_of_cpu ;
    $result_arr[$host->hostid]['reference'] = $host->opaquereference ;
    $result_arr[$host->hostid]['hostname' ] = $host->hostname ;

    $result_arr[$host->hostid]['vms' ]      = array() ;

    // Get the VM information for this host
    $query =  'SELECT * FROM {' . XCP_INSTANCES_TABLE . "} where `host_ip`='%s' " ;
    $query_args = array(
      $host->host_ip,
    ) ;
    $results_vm = db_query( $query, $query_args );

    $vm_arr = array() ;
    while ($vm_info = db_fetch_object($results_vm)) {

      $vm_arr['vmid'       ] =  $vm_info->vmid ;
      $vm_arr['vm_name'    ] =  $vm_info->vm_name ;
      $vm_arr['vm_ip'      ] =  $vm_info->vm_ip ;
      $vm_arr['vcpu_number'] =  $vm_info->vcpu_number ;

      $result_arr[$host->hostid]['vms' ][$vm_info->vmid] = $vm_arr ;
    }
  }

  return $result_arr ;
}


function _xcp_get_host_cpu_info_query() {

  $query   =  'SELECT c.* FROM ' . XCP_HOSTS_TABLE . '} c ' ;
  $results = db_query( $query, NULL );

  $result_arr = array() ;
  while ($host = db_fetch_object($results)) {

    $result_arr[$host->host_ip]              = array() ;
    $result_arr[$host->host_ip]['ip'       ] = $host->host_ip ;
    $result_arr[$host->host_ip]['no_of_cpu'] = $host->no_of_cpu ;
    $result_arr[$host->host_ip]['reference'] = $host->opaquereference ;
    $result_arr[$host->host_ip]['hostname' ] = $host->hostname ;
  }

  return $result_arr ;
}


function _xcp_get_snapshot_schedule_by_id($vm_id) {

  $query = 'SELECT c.* FROM {' . XCP_SNAPSHOT_SCHEDULE . "} c where vmid='%s'" ;
  $query_args = array(
    $vm_id,
  ) ;
  $result = db_query( $query, $query_args );

  $ret_result = array() ;
  while ($vm_record = db_fetch_object($result)) {

    $ret_result = $vm_record ;
  }

  return $ret_result ;
}


function _xcp_update_last_scheduled_snapshot($vm_id) {

  $update_query = 'UPDATE {' . XCP_SNAPSHOT_SCHEDULE . "} SET
                  `last_scheduled_snapshot`=CURRENT_TIMESTAMP
                  where `vmid`='%s' 
                  "; 

  $update_val = array(
    $vm_id ,
  ) ;
  
  $result = db_query( $update_query ,   $update_val );
}


function _xcp_update_snapshot_taken($vm_id) {
  $update_query = 'UPDATE {' . XCP_SNAPSHOT_SCHEDULE . "} SET
                  `last_snapshot_time`=CURRENT_TIMESTAMP
                  where `vmid`='%s' 
                  "; 

  $update_val = array(
    $vm_id ,
  ) ;
  
  $result = db_query( $update_query ,   $update_val );

}


function _xcp_update_snapshot_schedule($vm_id, $update_val) {

  $query      = 'SELECT c.* FROM {' . XCP_SNAPSHOT_SCHEDULE . "} c where vmid='%s'" ;
  $query_args = array(
    $vm_id ,
  ) ;
  
  $result = db_query( $query, $query_args );

  $record_found = FALSE ;

  while ($vm_record = db_fetch_object($result)) {

    $record_found = TRUE ;
  }

  $update_val[] = $vm_id ;
  if ($record_found) { // Update Record{
    $update_query = 'UPDATE {' . XCP_SNAPSHOT_SCHEDULE . "} SET
                  `monday_check`='%s'   , `monday_time_hh`='%s'   , `monday_time_mm`='%s'   , 
                  `tuesday_check`='%s'  , `tuesday_time_hh`='%s'  , `tuesday_time_mm`='%s'  , 
                  `wednesday_check`='%s', `wednesday_time_hh`='%s', `wednesday_time_mm`='%s', 
                  `thursday_check`='%s' , `thursday_time_hh`='%s' , `thursday_time_mm`='%s' , 
                  `friday_check`='%s'   , `friday_time_hh`='%s'   , `friday_time_mm`='%s'   , 
                  `saturday_check`='%s' , `saturday_time_hh`='%s' , `saturday_time_mm`='%s' , 
                  `sunday_check`='%s'   , `sunday_time_hh`='%s'   , `sunday_time_mm`='%s'  
                  where `vmid`='%s' 
                  "; 

    $result = db_query( $update_query , $update_val );
  }
  else { // Insert Record{
    $insert_query = 'INSERT INTO {' . XCP_SNAPSHOT_SCHEDULE . "} 
                  ( `monday_check`   , `monday_time_hh`    , `monday_time_mm`   ,  
                    `tuesday_check`  , `tuesday_time_hh`   , `tuesday_time_mm`  ,  
                    `wednesday_check`,  `wednesday_time_hh`, `wednesday_time_mm`, 
                    `thursday_check` , `thursday_time_hh`  , `thursday_time_mm` , 
                    `friday_check`   , `friday_time_hh`    , `friday_time_mm`   , 
                    `saturday_check` , `saturday_time_hh`  , `saturday_time_mm` , 
                    `sunday_check`   , `sunday_time_hh`    , `sunday_time_mm`   , 
                    `vmid` ,   `last_scheduled_snapshot` 
                  )  values 
                  ( 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', '%s', '%s', 
                  '%s', DATE_SUB(CURRENT_TIMESTAMP, INTERVAL 1 DAY )
                  ) 
          "; 

    $result = db_query( $insert_query , $update_val );
  }
}


function _xcp_get_snapshot_schedule_by_weekday($day, $hr, $mm) {

  $day_str_dbcol  = $day . '_check'   ;
  $hh_str_dbcol   = $day . '_time_hh' ;
  $mm_str_dbcol   = $day . '_time_mm' ;

  // Here we have need to add conditions for - and + (Check for +-1 minutes: The cron should not miss any Snapshot)
  $query = 'SELECT c.* FROM {' . XCP_SNAPSHOT_SCHEDULE . "} c
            where $day_str_dbcol='1' 
            and ( ( %s - $hh_str_dbcol ) = 0 ) 
            and ( ( %s - $mm_str_dbcol ) < 2 ) 
            and ( ( %s - $mm_str_dbcol ) > -2 )  
            and TIMESTAMPDIFF(MINUTE,  `last_scheduled_snapshot`,  CURRENT_TIMESTAMP ) > 5 " ;

  $query_args = array(
    $hr ,
    $mm ,
    $mm ,
  ) ;

  $result = db_query( $query, $query_args );

  $ret_result = array() ;
  while ($ss_record = db_fetch_object($result)) {

    $ret_result[] = $ss_record ;
  }

  return $ret_result ;
}


function _xcp_get_vm_by_reference($opaquereference) {
    
  $query        = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE opaquereference='%s'" ;
  $query_args   = array() ;
  $query_args[] = $opaquereference ;
  $db_result    = db_query($query , $query_args );

  while ($row = db_fetch_object($db_result)) {
    return $row ;
  }

  return NULL ;
}


function _xcp_get_vm_by_id($uuid) {

  $query = 'SELECT c.* FROM {' . XCP_INSTANCES_TABLE . "} c WHERE vmid='%s'" ;
  $query_args = array() ;
  $query_args[] = $uuid ;
  $db_result = db_query($query , $query_args );

  while ($row = db_fetch_object($db_result)) {
    return $row->vm_name ;
  }

  return '' ;
}


function _xcp_download_graph_info() {

  $arr = _xcp_get_all_running_vms() ;
  if (empty($arr)) { // Try once again{
    $arr = _xcp_get_all_running_vms() ;
    if (empty($arr) ) {
      return ;
    }
  }

  $insert_query = 'INSERT INTO {' . XCP_MONITOR_INFO_TABLE . '}
                  ( `vm_ip`,  
                    `display_monitor` 
                  )  values ';

  $count = 0 ;
  $query_args = array() ;

  $ip_str = $arr[0] ;
  $ip_arr = explode(',', $ip_str ) ;

  foreach ($ip_arr as $hostname) {

    $url = '' . XCP_MONITOR_URL ;
    if (empty($url ) || $url  === 'http://' ) {

        return ;
    }

    // XCP_MONITOR_URL is defined as 'xcp_monitor_url' in the admin settings form of xcp viz. xcp_get_admin_settings
    $chck_file_name = XCP_MONITOR_URL . "?hostname=$hostname;plugin=xcp_rrd_plugin;type=percent;type_instance=cpu0;begin=-3600;TmpParam=" . time()  ;
    if ( file($chck_file_name) ) {
      $graph_found = 1 ;
    }
    else {
      $graph_found = 0 ;
    }

    $query_args[] = $hostname ;
    $query_args[] = $graph_found ;

    $insert_query = $insert_query . "  (  '%s', '%s' ) ," ;
  }

  if (empty($query_args)) {
    return ;
  }

  $insert_query = substr($insert_query, 0, -1);
  db_query('truncate table {' . XCP_MONITOR_INFO_TABLE . '}', NULL );
  db_query( $insert_query, $query_args );
}


function _xcp_get_vm_monitor_info() {

  $query   = 'SELECT c.* FROM {' . XCP_MONITOR_INFO_TABLE . '} c ' ;
  $results = db_query( $query, NULL );

  $result_arr = array() ;
  while ($monitor_info = db_fetch_object($results)) {

    $result_arr[$monitor_info->vm_ip ]      = $monitor_info->display_monitor ;
  }

  if (empty($result_arr)) { // Check if its empty try once again{
    $results = db_query( $query ,   NULL );

    while ($monitor_info = db_fetch_object($results)) {

      $result_arr[$monitor_info->vm_ip ]      = $monitor_info->display_monitor ;
    }
  }

  return $result_arr ;
}


function _xcp_get_describe_snapshot_query() {

  return $query = 'SELECT c.* FROM {' . XCP_SNAPSHOT_TABLE . "} c WHERE  snapshot_of='%s' order by snapshot_time desc " ;
}

function _xcp_get_all_dns() {

  $db_result = db_query('select vm_ip as dns_name from {' . XCP_INSTANCES_TABLE . '}');
  $db_dns = array();
  while ($dns = db_fetch_object($db_result)) {
    $db_dns[] = $dns->dns_name;
  }
  return $db_dns;

}


function _xcp_save_terminate_instance($instance_id) {

  $sql_instance_update = 'update {' . XCP_INSTANCES_TABLE . "} set vm_state='%s' where vmid= '%s'" ;
  $query_args = array(
    'Halted'     ,
    $instance_id ,
  ) ;
  db_query( $sql_instance_update ,   $query_args );

  // User Activity Log
  cloud_audit_user_activity( array(
      'type'    => 'user_activity',
      'message' => t('Instance has been terminated: @instance_id', array('@instance_id' => $instance_id)),
      'link'    => '',
    )
  );

}

function _xcp_get_pool_name() {
  
  $xcp_pool_query = 'select * from {' . XCP_POOLS_TABLE . '}' ;
  $result         = db_query( $xcp_pool_query );

  while ($key = db_fetch_object($result)) {

    return $key->poolname ;
  }
}

function _xcp_get_pool_options() {

  $xcp_pool_query = 'select * from {' . XCP_POOLS_TABLE . '}';
  $result         = db_query( $xcp_pool_query );

  $pool_arr = array() ;
  while ($key = db_fetch_object($result)) {

    $pool_arr[$key->poolname] =  $key->poolname ;
  }

  return $pool_arr ;
}


function _xcp_get_network_options() {

  $xcp_network_query = 'select * from {' . XCP_NETWORK_TABLE . '}
    where opaquereference not in ( select distinct network_uuid from {' . XCP_PIF_TABLE . '} where bond_slave_of <> "OpaqueRef:NULL" ) order by bridge';
  
  $result            = db_query( $xcp_network_query );

  $network_arr = array() ;
  while ($key = db_fetch_object($result)) {

    $bridge = $key->bridge ;
    if ( strpos( $bridge , 'xapi0' ) !== FALSE ) { // Skip the xapi0 element

      continue ;
    }
    $network_arr[$key->uuid] = $bridge ;
  }

  return $network_arr ;
}

function _xcp_get_image_options($cloud_context) {

  $image_options = array() ; 
  
  $result = db_query('SELECT * FROM {' . XCP_INSTANCES_TABLE . "} where assigned_to = '' and vm_state='Halted' ");
  while ($nodes = db_fetch_object($result)) {
  
    if ( empty($nodes->host_ip) ) {  // Host Ip is empty
    
      $image_options[$nodes->vmid] = $nodes->vm_name ;
    }
    else {
    
      $image_options[$nodes->vmid] = $nodes->vm_name . ' ( ' .  $nodes->host_ip . ' )';
    }
  }
  
  $image_options[XCP_CLOUD_NONE] = XCP_CLOUD_NONE ;
  asort( $image_options);
  return $image_options ;
}


function _xcp_vm_info_update_db($vmid , $sql_col , $val) {

  $update_query = 'update ' . XCP_INSTANCES_TABLE . "  set %s='%s' where vmid='%s' " ;
  $query_args = array(
    $sql_col ,
    $val     ,
    $vmid    ,
  ) ;

  return db_query( $update_query, $query_args );
}


function _xcp_session_get() {

  // Return the latest session info
  $select_query = 'SELECT * FROM {' . XCP_CONSOLE_SESSION_TABLE . '} order by created desc limit 1 ' ;
  $result       = db_query( $select_query , NULL );

  while ($ses = db_fetch_object($result)) {

    return $ses->session_id ;
  }
}

function _xcp_session_set() {

  // Create a new Session ID and save it
  $insert_query = 'INSERT INTO {' . XCP_CONSOLE_SESSION_TABLE . '}
                  ( `session_id`,
                    `created`
                  )  values ( \'%s\' , \'%s\' ) ';

  $sesid = _xcp_login() ;
  if (empty($sesid) ) { // Do not process the request

      return ;
  }
  
  $query_args = array(
    $sesid    ,
    date('c') , // UTC
  );

  db_query( $insert_query, $query_args );
  

  // Logout/ Remove the other old session ids
  $select_query_args   = array() ;
  $select_query_args[] = date('c') ; // UTC

  // Session created before 3 mins log out from those
  $select_query         = 'SELECT * FROM {' . XCP_CONSOLE_SESSION_TABLE . "} where TIMESTAMPDIFF(MINUTE, created , '%s' ) > " . XCP_CONSOLE_SESSION_TIMEOUT  ;
  $result = db_query( $select_query , $select_query_args );

  $sess_to_delete = array() ;
  while ($ses = db_fetch_object($result)) {

    _xcp_logout( $ses->session_id ) ; // Logout
    $sess_to_delete[] = " '" . $ses->session_id . "' " ;
  }

  
  if (empty($sess_to_delete) === FALSE ) { // There are sessions to be delete

    $sess_str      = implode( ','  , $sess_to_delete ) ;
    $delete_query  = 'delete FROM {' . XCP_CONSOLE_SESSION_TABLE . "} where session_id in ( $sess_str ) " ;
  
    $result_delete = db_query( $delete_query , NULL );
  }
}

/**
 * Get the Lock status using the vmid
 * In case record is not present then return Unlock (This is the default value)
 * 
 * @param $vmid : VM whos safety status is to be returned
 */
function _xcp_get_lock_status($vmid) {

  $lock_query = 'select * from {' . XCP_INSTANCES_LOCK_TABLE . "} where instance_id='%s'  " ;

  $lock_status = 'Unlock' ;
  $query_args = array() ;
  $query_args[] = $vmid ;
  
  $result = db_query( $lock_query, $query_args );
  
  while ($key = db_fetch_object($result)) {

    $lock_status = $key->is_locked ;
  }
  
  return $lock_status ;
}

/**
 *
 * @return The the Lock status of all instances
 */
function _xcp_get_lock_status_all() {

  $lock_query = 'select * from {' . XCP_INSTANCES_LOCK_TABLE . "} " ;

  $lock_status = array() ;
  $query_args = array() ;
 
  $result = db_query( $lock_query, $query_args );

  while ($key = db_fetch_object($result)) {

    $lock_status[$key->instance_id] = $key->is_locked ;
  }

  return $lock_status ;
}


/**
 *
 * Set the Lock status of vm.
 * In case the vmid is not present then insert record else update
 * 
 * @param $vmid
 * @param $lock_status
 * @return
 */
function _xcp_set_lock_status($vmid, $lock_status) {

  $lock_query = 'select * from {' . XCP_INSTANCES_LOCK_TABLE . "} where instance_id='%s'  " ;
  $query_args = array() ;
  $query_args[] = $vmid ;

  $key_found = FALSE ;
  
  $result = db_query( $lock_query, $query_args );

  while ($key = db_fetch_object($result)) {

    $key_found = TRUE ;
  }

  if ($key_found) { // Update the Lock Status

      $update_query = "UPDATE {" . XCP_INSTANCES_LOCK_TABLE . "}
                    SET `is_locked`='%s'
                    where `instance_id`='%s' ";

      $update_args = array() ;
      $update_args[] =  $lock_status ;
      $update_args[] =  $vmid ;      

      $result_update = db_query( $update_query , $update_args );
      
  }
  else { // Insert record

      $insert_query = "INSERT INTO {" . XCP_INSTANCES_LOCK_TABLE . "}
                  ( `instance_id`,
                    `is_locked`
                  )  values ( '%s' , '%s' ) ";

      $insert_args = array() ;
      $insert_args[] =  $vmid ;
      $insert_args[] =  $lock_status ;

      $result_insert = db_query( $insert_query , $insert_args );
  }

  return TRUE ;
}

