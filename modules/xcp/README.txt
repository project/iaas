BASIC INFO
==========

- This module is built for Xen Cloud Platform (XCP)
  in order to manage XCP cloud through browser.
- You can still use OpenXenManager, but this project's goal is
  to realize hybrid cloud among XCP and other clouds like Amazon EC2 or
  OpenStack seamlessly
- Cloud module necessary
- Monitoring capability by collectd is also provided; with our orginal
  collectd plugin by: xcp_rrd_plugin.py


HOW TO USE
==========

- Enable XCP module (Requires Cloud module), then

  Clouds | XCP -> Click - Refresh Page -
  
- You also need to Setup

  - cron.php and
  - drupal_queue_cron.php
  
  on Drupal for automatic update for the latest information on XCP.


DIRECTORY STRUCTURE
===================

iaas
  +-modules
    +-xcp             (depends on cloud module)


INSTALLATION
============

* Drupal Queue
  Please download and install Drupal Queue module (drupal_queue)
  Set up cron like: wget http://YOUR_SERVER/drupal_queue_cron.php

* IPv4
  # wget http://pear.php.net/package/Net_IPv4/download and put it into /sites/all/modules/xcp/IPv4.php or
  # pear install Net_IPv4-1.3.4 

* RRDUpdates.py
  # wget http://wiki.xensource.com/xenwiki/XAPI_RRDs

* XenAPI.py XenAPI python
  # wget http://docs.vmd.citrix.com/XenServer/4.0.1/api/client-examples/python/XenAPI.py
  Copy this .py file to /sites/all/modules/xcp/XenAPI.py

* XenServerConsole.jar
  This jar is present on the XCP Host at the location /opt/xensource/debug/www/XenServerConsole.jar
  Copy this jar to /sites/all/modules/xcp/XenServerConsole.jar
  Or download: http://community.citrix.com/download/attachments/38633496/XenServerConsole-src.zip?version=1


KNOWN ISSUES
============
* Server Template will be shown but that functionality is not implemented (as a future work)


CHANGE HISTORY
==============
2011/06/02 ver.0.92 released 6.x-1.x-dev
2011/04/05 ver.0.91 released to reviewing process of drupal.org
2011/03/24 ver.0.9  released to reviewing process of drupal.org
2011/01/29 ver.0.82 released to reviewing process of drupal.org
2010/12/26 ver.0.81 released to reviewing process of drupal.org
2010/12/15 ver.0.8  released to reviewing process of drupal.org
2010/11/09 ver.0.7  released to reviewing process of drupal.org


Copyright
=========

Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.

End of README.txt