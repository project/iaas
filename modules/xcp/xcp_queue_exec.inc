<?php 

/**
 * @file
 * XCP Q Execute File.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */


/**
 * Execute the VM Shutdown task
 * 
 * @param  $task_details
 * @return
 */
function _xcp_exec_shutdown_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_shutdown_vm($uuid) ;
  $qid = $task_details['qid'] ;
  if ($result) {
    _xcp_update_task_status($qid, "Successfully terminated $uuid ", XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to terminate $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Hard Shutdown task
 * 
 * @param $task_details
 * @return 
 */
function _xcp_exec_hard_shutdown_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed ID is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_hard_shutdown_vm($uuid) ;

  $qid = $task_details['qid'] ;
  if ($result) {
    _xcp_update_task_status($qid, "Successfully terminated $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, 'Failed to terminate $uuid ', XCP_TASK_STATUS_FAILED) ;
  } 
} 

/**
 * Execute the Hard Reboot task
 * 
 * @param $task_details
 * @return 
 */
function _xcp_exec_hard_reboot_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed ID is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_hard_reboot_vm($uuid) ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  $qid = $task_details['qid'] ;
  if ($result) {
    _xcp_update_task_status($qid, "Successfully rebooted $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to reboot $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 

/**
 * Execute the Reboot task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_reboot_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed ID is empty ' , XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_reboot_vm($uuid) ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  $qid = $task_details['qid'] ;
  if ($result) {
    _xcp_update_task_status($qid, "Successfully rebooted $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to reboot $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 

/**
 * Execute the Suspend task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_suspend_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed ID is empty ', XCP_TASK_STATUS_FAILED) ;
    return FALSE ;
  } 

  $uuid = $task_details['uuid'] ;

  $result = _xcp_suspend_vm($uuid) ;

  $qid = $task_details['qid'] ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  if ($result) {
    _xcp_update_task_status($qid, "Successfully suspended $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to suspend $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Launch task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_launch_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;

  if (empty($task_details['data']) == FALSE) {
    $data_arr = _xcp_get_data($task_details['data']) ;
    if (empty($data_arr['host'])) {
      $result = _xcp_start_vm_default($uuid) ;
    }
    else {
      $result = _xcp_start_vm($uuid , $data_arr['host']) ;
    } 
  }
  else {
    $result = _xcp_start_vm_default($uuid) ;
  } 

  $qid = $task_details['qid'] ;

  if ($result) {
    _xcp_update_task_status($qid, "Successfully launched $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to launch $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Resume task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_resume_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_resume_vm($uuid) ;
  $qid = $task_details['qid'] ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  if ($result) {
    _xcp_update_task_status($qid, "Successfully launched $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to launch $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Revert Snapshot task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_revert_ss_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $result = _xcp_revert_SS($uuid) ;
  $qid = $task_details['qid'] ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  if ($result) {
    _xcp_update_task_status($qid, "Successfully reverted $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to revert $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Clone task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_clone_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $data_arr = _xcp_get_data($task_details['data']) ;

  $result = _xcp_clone_vm($uuid , $data_arr['name']) ;

  $qid = $task_details['qid'] ;

  if ($result === 'IN_PROGRESS') { // Task is in Progress.
    return ;
  } 

  if ($result) {
    _xcp_update_task_status($qid, "Successfully cloned $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to clone $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Destroy task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_destroy_vm_task($task_details) {

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $data_arr = _xcp_get_data($task_details['data']) ;

  $result = _xcp_destroy_vm($uuid) ;

  $qid = $task_details['qid'] ;

  if ($result) {
    _xcp_update_task_status($qid, "Successfully destroyed $uuid " , XCP_TASK_STATUS_COMPLETE) ;
  }
  else {
    _xcp_update_task_status($qid, "Failed to destroy $uuid " , XCP_TASK_STATUS_FAILED) ;
  } 
} 


/**
 * Execute the Download information task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_download_vm_info($task_details) {

  if (empty($task_details)) {
    return FALSE ;
  } 

  $result = _xcp_download_instances_info_xcp() ;

  $qid = $task_details['qid'] ;
  if ($result) {
    _xcp_update_task_status($qid, 'Successfully downloaded VM Info ' , XCP_TASK_STATUS_COMPLETE) ;
    return TRUE ;
  }
  else {
    _xcp_update_task_status($qid, 'Failed to downloaded VM Info ' , XCP_TASK_STATUS_FAILED) ;
    return FALSE ;
  } 

  return FALSE ;
} 


/**
 * Execute the Snapshot task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_snapshot_vm($task_details) {

  if (empty($task_details)) {
    return FALSE ;
  } 

  $uuid = $task_details['uuid'] ;

  $query = _xcp_get_instance_by_id_query() ;
  $query_args = array(
    $uuid,
  );
  $results = db_query($query , $query_args);

  $vm = db_fetch_object($results) ;

  $result = _xcp_snapshot_vm($uuid, 'Backup_' . $vm -> vm_name . '_' . date('Ymd-Hs')) ;

  $qid = $task_details['qid'] ;

  if ($result) {
    _xcp_update_task_status($qid, 'Successfully completed Snapshot of VM ' , XCP_TASK_STATUS_COMPLETE) ;
    _xcp_update_snapshot_taken($uuid);
    return TRUE ;
  }
  else {
    _xcp_update_task_status($qid, 'Failed to take snapshot of VM ' , XCP_TASK_STATUS_FAILED) ;
    return FALSE ;
  } 

  return FALSE ;
} 



/**
 * Execute the Network Assign task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_assign_network_vm($task_details) {

  if (empty($task_details)) {
    return FALSE ;
  }

  // Get the latest information
  _xcp_download_vif_info_xcp() ;
  
  $uuid = $task_details['uuid'] ;

  $query = _xcp_get_instance_by_id_query() ;
  $query_args = array(
    $uuid,
  );
  $results = db_query($query , $query_args);
  $vm = db_fetch_object($results) ;
  $device = _xcp_get_next_device($vm->opaquereference) ;
  $result = _xcp_assign_network_vm( $vm->opaquereference , $task_details['data'] , $device ) ;

  $qid = $task_details['qid'] ;

  if ($result) {
    _xcp_update_task_status($qid, 'Successfully completed network assignment of VM ' , XCP_TASK_STATUS_COMPLETE) ;
    
    _xcp_download_vif_info_xcp() ;
    return TRUE ;
  }
  else {
    _xcp_update_task_status($qid, 'Failed to assign network to VM ' , XCP_TASK_STATUS_FAILED) ;
    return FALSE ;
  }

  return FALSE ;
}



/**
 * Execute the Release Network task
 *
 * @param $task_details
 * @return
 */
function _xcp_exec_release_network_vm($task_details) {

  if (empty($task_details)) {
    return FALSE ;
  }

  $vif_ref = $task_details['data'] ;

  $result = _xcp_release_network_vm( $vif_ref ) ;
  $qid = $task_details['qid'] ;

  if ($result) {
    _xcp_update_task_status($qid, 'Successfully completed network release ' , XCP_TASK_STATUS_COMPLETE) ;
    
    _xcp_download_vif_info_xcp() ;
    return TRUE ;
  }
  else {
    _xcp_update_task_status($qid, 'Failed to release network ' , XCP_TASK_STATUS_FAILED) ;
    return FALSE ;
  }

  return FALSE ;
}

/**
 * Check the Progress of the previously executed Task 
 *
 * @param $task_details
 * @return
 */
function _xcp_check_progress($task_details) {

  if ($task_details['task'] ===  XCP_VM_SUSPEND ) {
    _xcp_update_progress_status($task_details, 'Suspended') ;
  }
  elseif ($task_details['task'] ===  XCP_VM_RESUME ) {
    _xcp_update_progress_status($task_details, 'Running') ;
  }
  elseif ($task_details['task'] ===  XCP_VM_HARD_REBOOT ) {
    _xcp_update_progress_status($task_details, 'Running') ;
  }
  elseif ($task_details['task'] ===  XCP_VM_REBOOT ) {
    _xcp_update_progress_status($task_details, 'Running') ;
  } 
} 


/**
 * Update the Progress of previously added task
 *
 * @param $task_details
 * @return
 */
function _xcp_update_progress_status($task_details, $result_state) {

  $qid = $task_details['qid'] ;

  if (empty($task_details) || empty($task_details['uuid'])) {
    _xcp_update_task_status($task_details['qid'], 'Failed uuid is empty ', XCP_TASK_STATUS_FAILED) ;
    return ;
  } 

  $uuid = $task_details['uuid'] ;
  $power_state = _xcp_get_vm_power_state($uuid) ;

  if ($power_state === $result_state) {
    _xcp_update_task_status($qid, "Successfully $result_state VM" , XCP_TASK_STATUS_COMPLETE) ;
    return TRUE ;
  } 
} 

function _xcp_get_data($data) {

  $result_arr = array() ;
  $data_arr = explode(',', $data);
  foreach ($data_arr as $value) {
    $key_val = explode('=', $value);
    $result_arr[$key_val[0]] = $key_val[1] ;
  } 

  return $result_arr ;
} 
