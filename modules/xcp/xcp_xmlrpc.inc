<?php 

/**
 * @file
 * XCP XML-RPC call File.
 *
 * Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
 *
 */

 
require_once 'IPv4.php' ;

function _xcp_check_subnet($subnet, $ip) {

  if (empty($subnet)
  ||  empty($ip    )) { // In case subnet is not given
      return TRUE ;
  } 

  $obj_ip = new Net_IPv4();

  $result = $obj_ip -> ipInNetwork($ip, $subnet) ;

  return $result ;
} 


/**
 * Send request to Master Host for getting the information
 * Save the information in the database 
 * @return
 */
function _xcp_get_info() {

  
  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return array() ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;

  $params = array() ;

  foreach ($all_records as $key => $VM) {

    if ($VM['is_a_template'    ] == FALSE
    &&  $VM['is_a_snapshot'    ] == FALSE
    &&  $VM['is_control_domain'] == FALSE) {
      $name          = $VM['name_label'] ;
      $vm_state       = $VM['power_state'] ;
      $vm_id         = $VM['uuid'] ;

      $guest_metrics = $VM['guest_metrics'] ;
      $obj           = xmlrpc($xcp_host, 'VM_guest_metrics.get_networks', $sesid, $guest_metrics) ;

      $parent_host   = $VM['resident_on'] ;
      $host_ip_arr   = xmlrpc($xcp_host, 'host.get_address', $sesid, $parent_host) ;
      $host_ip       = isset( $host_ip_arr['Value'] ) ? $host_ip_arr['Value'] : '' ;

      $start_time    = '' ;
      $vm_ip         = '' ;
      $os            = '' ;
      $val_obj_vcpus = 0 ; // Number of VCPUs of a VM
      
      if ($obj['Status'] == 'Success' && $vm_state == 'Running') {
        $ips_arr = $obj['Value'] ;
        foreach ($ips_arr as $ip_details) {
          $vm_ip = $ip_details ;

          if (_xcp_check_subnet(XCP_MONITORING_SUBNET , $ip_details)) {
            break ;
          } 
        } 

        $obj_os = xmlrpc($xcp_host, 'VM_guest_metrics.get_os_version', $sesid, $guest_metrics) ;
        if ($obj_os['Status'] == 'Success') {
          $val_os = $obj_os['Value'] ;
          $os = $val_os['distro'] ;
        } 

        $obj_time = xmlrpc($xcp_host, 'VM_metrics.get_start_time', $sesid, $VM['metrics']) ;
        $obj_time_records = $obj_time['Value'] ;
        $start_time = $obj_time_records -> iso8601 ;
      } 

      $obj_vcpus = xmlrpc($xcp_host, 'VM_metrics.get_VCPUs_number', $sesid, $VM['metrics']) ;

      if ($obj_vcpus['Status'] == 'Success') {
        $val_obj_vcpus = $obj_vcpus['Value'] ;
      } 

      $params[] = $vm_id         ;
      $params[] = $name          ;
      $params[] = $vm_ip         ;
      $params[] = $host_ip       ;
      $params[] = $vm_state       ;
      $params[] = $key           ;
      $params[] = $os            ;
      $params[] = $start_time    ;
      $params[] = $val_obj_vcpus ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
} 

/**
 * Get the information related to Console of VMs
 * 
 * @return
 */
function _xcp_get_xcp_console_info() {

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;

  if ($sesid == FALSE) {
    return array() ;
  } 

  $obj = xmlrpc($xcp_host, 'console.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;

  $params = array() ;

  foreach ($all_records as $console) {
    $params[] = $console['uuid'    ] ;
    $params[] = $console['protocol'] ;
    $params[] = $console['location'] ;
    $params[] = $console['VM'      ] ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
} 


/**
 * Login into the Master XCP
 * 
 * @return
 */
function _xcp_login() {

  $xcp_host = variable_get(XCP_CONTEXT . '_host_uri'     , '');
  $user_id  = variable_get(XCP_CONTEXT . '_user_id'      , '');
  $passwd   = variable_get(XCP_CONTEXT . '_user_password', '');

  $obj = xmlrpc($xcp_host, 'session.login_with_password', $user_id , $passwd) ;
  $sesid = $obj['Value'] ;

  if (empty($sesid)) {

    if (user_access('administer xcp')) {    
    
        $admin_url = filter_xss( l( t('XCP Settings'), 'admin/settings/xcp' ) );
    }
    else {
        
        $admin_url = 'XCP' ;
    }
    drupal_set_message(check_plain(t('The variables are not correctly configured: ')) . $admin_url, 'error');
    return FALSE ;
  } 

  return $sesid ;
} 



function _xcp_logout($sesid) {

    if (empty($sesid) === FALSE ) {

        $xcp_host = variable_get(XCP_CONTEXT . '_host_uri'     , '');
        $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    }
}

/**
 * Start a VM
 *
 * @param  $uuid
 *          VM to be started
 * @param $host_ref
 *          Host on which the VM is to launched 
 * @return 
 */
function _xcp_start_vm($uuid, $host_ref) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid'   , $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid , $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Halted') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.start_on', $sesid , $vmref , $host_ref , $start_paused , $force) ;
    $start_result = $obj['Status'] ; 
    // print_r($obj);die;
    if ($start_result === 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) === FALSE) {
      $result = FALSE ;
      if ($start_result === "Failure") {
        $err_desc = $obj['ErrorDescription'] ;
        $err_msg = '' ;
        foreach ($err_desc as $err) {
          $err_msg  .= $err . "\n" ;
        } 

        cloud_log_to_db("LAUNCH:FAILED", 'UUID=' . $uuid . ":Error:" . $err_msg);
      } 
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
}

/**
 * Launch the VM on default selected Host
 * 
 * @param $uuid
 *          Id of Vm to be launched 
 * @return 
 */
function _xcp_start_vm_default($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Halted') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.start', $sesid, $vmref , $start_paused , $force) ;
    $start_result = $obj['Status'] ;

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
      if ($start_result === 'Failure') {
        $err_desc = $obj['ErrorDescription'] ;
        $err_msg = '' ;
        foreach ($err_desc as $err) {
          $err_msg  .= $err . "\n" ;
        } 

        cloud_log_to_db("LAUNCH_USER:FAILED", "UUID=" . $uuid . ":Error:" . $err_msg);
      } 
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Shutdown a VM
 * 
 * @param $uuid
 *          Id of VM to be shutdown.
 * @return
 */
function _xcp_shutdown_vm($uuid) {

  // update termidated instance time under instance details table
  cloud_billing_update(XCP_CONTEXT);
  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Running'
  ||  $power_state == 'Suspended') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.clean_shutdown', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
      if ($start_result === "Failure") {
        $err_desc = $obj['ErrorDescription'] ;
        $err_msg = '' ;
        foreach ($err_desc as $err) {
          $err_msg  .= $err . "\n" ;
        } 

        cloud_log_to_db("TERMINATE:FAILED", 'UUID=' . $uuid . ":Error:" . $err_msg);
      } 
    } 
  }
  else { // VM is already down
      $result = TRUE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 

/**
 * Send a request to Hard Shutdown a VM
 * 
 * @param $uuid
 *          Id of VM to be terminated
 * @return 
 */
function _xcp_hard_shutdown_vm($uuid) {

  // update termidated instance time under instance details table
  cloud_billing_update(XCP_CONTEXT,
                       $uuid,
                       'terminated'
                     );
  // db_query('update {' . CLOUD_BILLING_INSTANCES_DETAILS_TABLE . "} set instancestatename='Halted', terminated_date='" . date("c") . "' where instanceid= '" . $uuid . "'");
  // ///
  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Running'
  ||  $power_state == 'Suspended') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.hard_shutdown', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  }
  else { // VM is already down
      $result = TRUE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to Destroy a VM
 * 
 * @param $uuid
 * @return
 */
function _xcp_destroy_vm($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Halted') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.destroy', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to Suspend a VM
 *
 * @param $uuid
 * @return
 */
function _xcp_suspend_vm($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Running') {
    $obj = xmlrpc($xcp_host, 'VM.suspend', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if (empty($obj)) {
      $result = 'IN_PROGRESS' ; //Empty indicates the Process is still running.
    } 

    if ($start_result === 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  }
  else {
    $result = TRUE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to resume a VM
 *
 * @param $uuid
 * @return
 */
function _xcp_resume_vm($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');

  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Suspended') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.resume', $sesid, $vmref, FALSE, FALSE) ;
    $start_result = $obj['Status'] ;

    if (empty($obj)) {
      $result = 'IN_PROGRESS' ;
    } 

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to revert a Snapshot
 *
 * @param $uuid
 * @return
 */
function _xcp_revert_SS($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');

  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.revert', $sesid, $vmref) ;
  $start_result = $obj['Status'] ;

  if (empty($obj)) {
    $result = 'IN_PROGRESS' ;
  } 

  if ($start_result == 'Success') {
    $result = TRUE ;
  }
  elseif (empty($obj) == FALSE) {
    $result = FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to Hard Reboot 
 *
 * @param $uuid
 * @return
 */
function _xcp_hard_reboot_vm($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Running' || $power_state == 'Suspended') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.hard_reboot', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if (empty($obj)) {
      $result = 'IN_PROGRESS' ;
    } 

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to Reboot
 *
 * @param $uuid
 * @return
 */
function _xcp_reboot_vm($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Running' || $power_state == 'Suspended') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.clean_reboot', $sesid, $vmref) ;
    $start_result = $obj['Status'] ;

    if (empty($obj)) {
      $result = 'IN_PROGRESS' ;
    } 

    if ($start_result == 'Success') {
      $result = TRUE ;

      $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
      $power_state = $obj['Value'] ;
      _xcp_update_state_by_uuid($uuid , $power_state) ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a request to Clone
 *
 * @param $uuid
 * @return
 */
function _xcp_clone_vm($uuid, $vm_name) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  if ($power_state == 'Halted') {
    $start_paused = FALSE ;
    $force = FALSE ;

    $obj = xmlrpc($xcp_host, 'VM.clone', $sesid, $vmref , $vm_name) ;
    $start_result = $obj['Status'] ;

    if (empty($obj)) {
      $result = 'IN_PROGRESS' ;
    } 

    if ($start_result == 'Success') {
      $result = TRUE ;
    }
    elseif (empty($obj) == FALSE) {
      $result = FALSE ;
    } 
  }
  else {
    drupal_set_message(t('The VM should be in Halted state to be cloned.') , 'error');
    $result = TRUE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Get the Host information 
 * @return
 */
function _xcp_get_host_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'host.get_all_records' , $sesid) ;
  $host_set = $obj['Value'] ;

  $params = array() ;
  foreach ($host_set as $key => $host) {
    $cpu_set = $host['host_CPUs'] ;
    $no_of_cpu = sizeof($cpu_set) ;

    // <strike>TODO: Get more info (by Jamir)</strike>
    // [Jamir] Need to know what information is required. -> Full Information
    // TODO: Get full info (by Jamir)
    
    $params[] = $host['uuid'    ] ;
    $params[] = $host['hostname'] ;
    $params[] = $host['address' ] ;
    $params[] = $no_of_cpu        ;
    $params[] = $key              ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
  return $params ;
} 

/**
 * Get the Snapshot information
 * 
 * @return
 */
function _xcp_get_snapshot_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;

  $params = array() ;

  foreach ($all_records as $key => $VM) {
    if ($VM['is_a_template'] == TRUE && $VM['is_a_snapshot'] == TRUE && $VM['is_control_domain'] == FALSE) {
      $params[] = $VM['uuid'] ;
      $params[] = $VM['name_label'] ;
      $params[] = $VM['snapshot_of'] ;
      $params[] = $VM['snapshot_time'] -> iso8601 ;
    } 
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
  return $params ;
} 


/**
 * Rename a VM
 * 
 * @param $uuid
 *          Id of VM to be renamed
 * @param $new_names
 *          New name to be assigned 
 * @return
 */
function _xcp_rename_vm($uuid, $new_names) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.set_name_label', $sesid, $vmref , $new_names) ;
  $result = $obj['Status'] ;

  if ($result == 'Success')
    $result = TRUE ;

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send a snapshot request
 * 
 * @param  $uuid
 * @param  $ss_name
 * @return
 */
function _xcp_snapshot_vm($uuid, $ss_name) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.snapshot', $sesid, $vmref , $ss_name) ;
  $start_result = $obj['Status'] ;

  if ($start_result === 'Success') {
    $result = TRUE ;
  }
  elseif (empty($obj) === FALSE) {
    $result = FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $result ;
} 


/**
 * Send assign network request
 *
 * @param  $uuid
 * @param  $network_id
 * @return
 */
function _xcp_assign_network_vm($vm_ref, $network_id , $device ) {

  $result = FALSE ;
  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  $device_str = '' . $device ;
  $obj = xmlrpc($xcp_host, 'network.get_by_uuid', $sesid, $network_id) ;
  $network_ref = $obj['Value'] ;

  // Set the default values for creating VIF
  $params =  array(     'device'                    => $device_str ,
                        'network'                   => $network_ref,
                        'VM'                        => $vm_ref,
                        'MAC'                       => '',
                        'MTU'                       => '1500',
                        'qos_algorithm_type'        => '',
                        'qos_algorithm_params'      => array( '' => '' ),
                        'other_config'              => array( '' => '') ,
                    ) ;

  // Call create VIF
  $obj = xmlrpc( $xcp_host, 'VIF.create' , $sesid , $params ) ;

  $create_result = $obj['Status'] ;
  if ($create_result === 'Success') { // Successfully created VIF

      $vm = _xcp_get_vm_by_reference($vm_ref) ;
      if ($vm !== NULL && strtolower($vm->vm_state) !== 'running' ) {

          $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;  
          return TRUE ;
      }

      $vif_ref = $obj['Value'] ; // Reference of newly created VIF

      // Now attach this VIF to vm
      $obj = xmlrpc( $xcp_host, 'VIF.plug' , $sesid , $vif_ref) ;
      $plug_result = $obj['Status'] ;
      if ($plug_result === 'Success') {

          // Completed the operation successfully
          $result = TRUE ;
      }
  }

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
  return $result ;
}



/**
 * Send release network request
 *
 * @param  $uuid
 * @param  $network_id
 * @return
 */
function _xcp_release_network_vm($vif_ref) {

  $result = FALSE ;
  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  // Call detach VIF
  $obj = xmlrpc( $xcp_host, 'VIF.unplug' , $sesid , $vif_ref ) ;

  $unplug_result = $obj['Status'] ;
  if ($unplug_result === 'Success') { // Successfully unplugged VIF

      // Now destroy this VIF to vm
      $obj = xmlrpc( $xcp_host, 'VIF.destroy' , $sesid , $vif_ref) ;
      $plug_result = $obj['Status'] ;
      if ($plug_result === 'Success') {

          // Completed the operation successfully
          $result = TRUE ;
      }
  }
  else { // It is already unplugged

      // Now destroy this VIF to vm
      $obj = xmlrpc( $xcp_host, 'VIF.destroy' , $sesid , $vif_ref) ;
      $plug_result = $obj['Status'] ;
      if ($plug_result === 'Success') {

          // Completed the operation successfully
          $result = TRUE ;
      }
  }
  
  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
  return $result ;
}



/**
 * Get the current power state of VM
 *
 * @param   $uuid
 *              Id of VM to be queried
 * @return
 */
function _xcp_get_vm_power_state($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'VM.get_power_state', $sesid, $vmref) ;
  $power_state = $obj['Value'] ;

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $power_state ;
} 


function _xcp_get_all_ip($uuid) {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'VM.get_by_uuid', $sesid, $uuid) ;
  $vmref = $obj['Value'] ;

  $obj_guest_metrics = xmlrpc($xcp_host, 'VM.get_guest_metrics', $sesid, $vmref) ;
  $guest_metrics = $obj_guest_metrics['Value'] ;

  $obj_networks = xmlrpc($xcp_host, 'VM_guest_metrics.get_networks', $sesid, $guest_metrics) ;

  $ips_arr = isset( $obj_networks['Value'] ) ? $obj_networks['Value'] : FALSE ;
  $ret_arr = array() ;

  if (empty($ips_arr)) {
  
    $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    return FALSE ;
  } 

  foreach ($ips_arr as $ip_details) {
    $ret_arr[] = $ip_details ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $ret_arr ;
} 


/**
 * Get information about XCP pool
 * 
 * @return
 */
function _xcp_get_pool_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  } 

  $obj = xmlrpc($xcp_host, 'pool.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;
  $status = $obj['Status'] ;

  if ($status !== 'Success') {
  
    $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    // Failed
    return ;
  } 

  $params = array() ;
  foreach ($all_records as $key => $pool) {

    // <strike>TODO: Get more info (by Jamir)</strike>
    // [Jamir] Need to know what information is required. -> Full Information
    // TODO: Get full info (by Jamir)
    
    $params[] = $pool['uuid'      ] ;
    $params[] = $pool['name_label'] ;
    $params[] = $pool['master'    ] ;
  } 

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
}


/**
 * Get information about XCP Network
 *
 * @return
 */
function _xcp_get_network_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  $obj = xmlrpc($xcp_host, 'network.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;
  $status = $obj['Status'] ;

  if ($status !== 'Success') {
    // Failed
    $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    return ;
  }

  $params = array() ;
  foreach ($all_records as $key => $network) {

    $params[] = $network['uuid'      ] ;
    $params[] = $network['name_label'] ;
    $params[] = $network['name_description'] ;
    $params[] = $network['bridge'    ] ;
    $params[] = $key ;
  }

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
}


/**
 * Get information about XCP VIF
 *
 * @return
 */
function _xcp_get_vif_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  $obj = xmlrpc($xcp_host, 'VIF.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;
  $status = $obj['Status'] ;

  if ($status !== 'Success') {
    // Failed
    $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    return ;
  }

  $params = array() ;
  foreach ($all_records as $key => $vif) {

    $params[] = $key ; // vif_ref
    $params[] = $vif['uuid'      ] ;
    $params[] = $vif['device'] ;
    $params[] = $vif['network'] ;
    $params[] = $vif['VM'    ] ;
    $params[] = $vif['MAC'    ] ;
    $params[] = $vif['currently_attached'    ] ;
  }

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
}


/**
 * Get information about XCP PIF
 *
 * @return
 */
function _xcp_get_pif_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  $obj = xmlrpc($xcp_host, 'PIF.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;
  $status = $obj['Status'] ;

  if ($status !== 'Success') {
    // Failed
    $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
    return ;
  }

  $params = array() ;
  foreach ($all_records as $key => $pif) {

    $params[] = $pif['uuid'      ] ;
    $params[] = $pif['device'] ;
    $params[] = $pif['MAC'    ] ;
    $params[] = $pif['physical'    ] ;
    $params[] = $pif['currently_attached'] ;
    $params[] = $pif['MTU'    ] ;
    $params[] = '' ;
    $params[] = $pif['bond_slave_of'] ;
    $params[] = $pif['network'] ;
    $params[] = $pif['host'] ;
    $params[] = $key ; // pif_ref
  }

  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;

  return $params ;
}


/**
 * Get information about XCP VM Network
 *
 * @return
 */
function _xcp_get_vm_network_info() {

  $result = FALSE ;

  $xcp_host = variable_get('xcp_host_uri' , '');
  $sesid = _xcp_login() ;
  if ($sesid == FALSE) {
    return FALSE ;
  }

  $obj = xmlrpc($xcp_host, 'VM.get_all_records' , $sesid) ;
  $all_records = $obj['Value'] ;

  $params = array() ;

  foreach ($all_records as $key => $VM) {

    if ($VM['is_a_template'    ] == FALSE
    &&  $VM['is_a_snapshot'    ] == FALSE
    &&  $VM['is_control_domain'] == FALSE) {
        
      $vm_state      = $VM['power_state'] ;
      $vm_id         = $VM['uuid'] ;

      $guest_metrics = $VM['guest_metrics'] ;
      $obj           = xmlrpc($xcp_host, 'VM_guest_metrics.get_networks', $sesid, $guest_metrics) ;

      if ($obj['Status'] == 'Success' && $vm_state == 'Running') {
          
        $ips_arr = $obj['Value'] ;
        foreach ($ips_arr as $device => $ip_details) {

            $params[] = $vm_id  ;
            $params[] = $device  ;
            $params[] = $ip_details  ;
        } // End of for $ips_arr
      } // Running check
    } // End of if
  } // End of foreach
  
  $obj = xmlrpc($xcp_host, 'session.logout' , $sesid) ;
  
  return $params ;
}
