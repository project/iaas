#$id$

##########################################################################
#
# Copyright (c) 2010-2011 DOCOMO Communications Laboratories USA, Inc.
#
##########################################################################

# Plugin to read rrd data of XCP Host
# XCP Host maintains rrd data for VMs,  this plugin fetches the data and put it in the RRD of collectd


import collectd
import time
from RRDUpdates import RRDUpdates

HOST_LIST    = []
VM_LIST      = {}
XCP_USERID   = '' 
XCP_PASSWORD = '' 
MASTER       = '' 

#== Our Own Functions go here: ==#
def configure(conf):
  collectd.debug('Configuring Stuff')
  global HOST_LIST,  VM_LIST,  XCP_USERID,  XCP_PASSWORD,  MASTER

  for node in conf.children:
    if node.key == 'Host':
      HOST_LIST.append(node.values[0])
    elif node.key == 'VM':
      VM_LIST[ node.values[0] ] = node.values[1] 
    elif node.key == 'XCP_UserId':
      XCP_USERID = node.values[0] 
    elif node.key == 'XCP_Password':
      XCP_PASSWORD = node.values[0] 
    elif node.key == 'Master':
      MASTER = node.values[0] 


def callback_initer():
  collectd.debug('Initialisation')

def callback_reader(input_data=None):
  
  global HOST_LIST,  VM_LIST,  XCP_USERID,  XCP_PASSWORD,  MASTER
  value=0
  collectd.info('Started Collecting data (TIME)= %d \n\n'%( int(time . time()) ,  ) )

  for host in HOST_LIST:
    
    rrd_updates = RRDUpdates()
    rrd_updates.getXCP_VMData( MASTER,  host ,  XCP_USERID ,  XCP_PASSWORD )

    for uuid in rrd_updates.get_vm_list():
      for param in rrd_updates.get_vm_param_list(uuid):
        for row in range(rrd_updates.get_nrows()):

          if ( VM_LIST.get( uuid ,  0) == 0 ):
            continue

          if ( param == 'cpu0'):  
            v2 = collectd.Values(type='percent')
            value=(int)(rrd_updates.get_vm_data(uuid, param, row) * 100 )
            collectd.info('PARAM::DATA %d'%( value ) )    
          else:  
            v2 = collectd.Values(type='gauge')
            value=rrd_updates.get_vm_data(uuid, param, row)

          v2.host = VM_LIST[uuid]
          v2.plugin='xcp_rrd_plugin'
          v2.type_instance=param
          v2.time=rrd_updates.get_row_time(row)
          v2.dispatch(values=[value])
          #collectd.info('Info of::: %s\t%s\t%s\t%s\n'%( v2.host,  v2.type_instance,  v2.time,  rrd_updates.get_vm_data(uuid, param, row) ) )
  collectd.info('Done:: Collecting data (TIME)= %d \n\n'%( int(time . time()) ,  ) )
  
  
#== Hook Callbacks,  Order is important! ==#
collectd.register_config(configure)
collectd.register_init(callback_initer)
collectd.register_read(callback_reader,  30 ) # Execute read after every 30 seconds 
